import React, { Component } from "react";
import _ from "lodash";
import Sorter from "./Sorter";
import Search from "./Search";
import { fire } from "../firebase/firebase";
import CircularProgress from "@material-ui/core/CircularProgress";
import axios from "axios";

export default class Distributors extends Component {
  constructor() {
    super();
    this.state = {
      searchInput: "",
      distributors: [],
      sorted: "asc",
    };
  }

  fetchUserData = () => {
    console.log("fetched");
    fire
      .database()
      .ref("/users")
      .once("value")
      .then((snapshot) => {
        const sortedUserData = _.sortBy(snapshot.val(), [
          (o) => {
            return o.company;
          },
        ]);
        this.setState({
          distributors: sortedUserData,
        });
      });
  };

  sortSuccess = (sortedData) => {
    this.setState({
      distributors: sortedData,
      sorted: this.state.sorted === "asc" ? "desc" : "asc",
    });
  };

  componentDidMount() {
    this.fetchUserData();
  }
  handleChange = (event) => {
    this.setState({
      searchInput: event.target.value,
    });
  };

  renderSaveBar() {
    return (
      <Search
        handleChange={this.handleChange}
        search={this.state.searchInput}
      />
    );
  }
  deleteUser = (save) => {
    console.log(save);
    fire
      .database()
      .ref("/users/" + save.uid)
      .remove();
  };
  updateUser = (save) => (e) => {
    fire
      .database()
      .ref("/users/" + save.uid)
      .update({
        distributorStatus: e.target.value,
      })
      .then(() => this.fetchUserData());

    axios.post(
      "https://api.hsforms.com/submissions/v3/integration/submit/4135982/d89915f1-e0d5-4e55-9a38-8eb5bcb48e1b",
      {
        fields: [
          {
            name: "email",
            value: save.email,
          },
          {
            name: "firstname",
            value: save.displayName,
          },
          {
            name: "company",
            value: save.company,
          },
          {
            name: "iris_calculator_user",
            value: true,
          },
          {
            name: "contact_type_base",
            value: "Dealer",
          },
        ],
        captchaEnabled: false,
      }
    );
  };

  renderSaves() {
    const { distributors } = this.state,
      search = this.state.searchInput.toLowerCase();
    let searchResults = _.filter(distributors, function (o) {
      let distributorStatus = "",
        company = "",
        displayName = "",
        email = "",
        uid = "";
      if (o.displayName !== undefined) {
        displayName = o.displayName.toLowerCase();
      }
      if (o.company !== undefined) {
        company = o.company.toLowerCase();
      }
      if (o.email !== undefined) {
        email = o.email.toLowerCase();
      }
      if (o.uid !== undefined) {
        uid = o.uid;
      }
      if (o.distributorStatus !== undefined) {
        distributorStatus = o.distributorStatus;
      }

      const data = displayName + company + distributorStatus + email + uid;

      return _.includes(data, search);
    });

    if (Object.keys(distributors).length === 0) {
      return (
        <div>
          <CircularProgress />
        </div>
      );
    } else {
      return _.map(searchResults, (saves) => {
        return (
          <div className="save-database" key={saves.title}>
            <div className="flex-container">
              <div className="save-box">
                <div className="columns">
                  <div className="column">
                    <div className="save-file">{saves.company}</div>
                  </div>
                  <div className="column">
                    {saves.displayName} ({saves.distributorStatus})
                  </div>
                  <div className="column">{saves.email}</div>
                  <div className="column">
                    <div className="save-ui">
                      <select
                        id="user-switch"
                        value={saves.distributorStatus}
                        onChange={this.updateUser(saves)}
                      >
                        <option value="End User">End User</option>
                        <option value="Pending Dealer">Pending Dealer</option>
                        <option value="Dealer">Dealer</option>
                      </select>
                      <i
                        onClick={() => this.deleteUser(saves)}
                        className="far fa-trash-alt"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );

        // return _.map(saves.savedData, sD => {
        //
        // });
      });
    }
  }

  render() {
    const { distributors, sorted } = this.state;
    return (
      <div
        className="container"
        style={{ padding: "100px 0", minHeight: "100vh" }}
      >
        <div className="product-settings">
          <h2>Users</h2>
        </div>
        {this.renderSaveBar()}
        {Object.keys(distributors).length !== 0 && (
          <div className="columns top-sort">
            <div className="column">
              Company
              <Sorter
                sorted={sorted}
                callback={this.sortSuccess}
                data={distributors}
                sortBy="company"
              />
            </div>
            <div className="column">
              Contact{" "}
              <Sorter
                sorted={sorted}
                sortBy="displayName"
                callback={this.sortSuccess}
                data={distributors}
              />
            </div>
            <div className="column">
              Email
              <Sorter
                sorted={sorted}
                sortBy="email"
                callback={this.sortSuccess}
                data={distributors}
              />
            </div>
            <div className="column">
              Status
              <Sorter
                sorted={sorted}
                sortBy="distributorStatus"
                callback={this.sortSuccess}
                data={distributors}
              />
            </div>
          </div>
        )}

        {this.renderSaves()}
      </div>
    );
  }
}
