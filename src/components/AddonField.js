import React, { Component } from "react";
import _ from "lodash";
import TextField from "@material-ui/core/TextField";

class AddonField extends Component {
  componentDidMount() {
    console.log("test");
  }
  renderRemove = (i, length, type) => {
    const index = parseInt(i, 10);
    if (index !== 0) {
      return (
        <div
          style={{
            padding: "0 8px",
            width: "0px",
          }}
          className="btn"
          onClick={this.props.removeAddon(type, i)}
        >
          -
        </div>
      );
    }
  };

  renderAdd = (i, length, type) => {
    const index = parseInt(i, 10);
    if (index === length) {
      return (
        <div
          style={{
            padding: "0 8px",
          }}
          className="btn-add"
          onClick={() => this.props.appendInput(type, i)}
        >
          +
        </div>
      );
    }
  };

  renderOptions = (type) => {
    return _.map(this.props[type], (item, i) => {
      return (
        <option key={i} value={item.id}>
          {item.sku}
        </option>
      );
    });
  };

  renderPrice = (input, type, i) => {
    if (this.props.admin || this.props.distributorStatus === "Dealer") {
      if (this.props.config[type][i].adjustedPrice) {
        console.log("yso");

        return (
          <TextField
            style={{
              margin: "0px",
            }}
            autoComplete="off"
            id="standard-number"
            type="number"
            inputProps={{
              min: input.dealerPrice,
            }}
            margin="normal"
            value={input.dealerPrice}
            onChange={this.props.changeInputValue("adjustedPrice", i)}
          />
        );
      } else if (this.props.config[type][i].name === "None") {
        console.log("he");

        return false;
      } else {
        console.log("yo");
        return (
          <TextField
            style={{
              margin: "0px",
            }}
            autoComplete="off"
            id="standard-number"
            type="number"
            margin="normal"
            value={input.dealerPrice}
            onChange={this.props.changeInputValue("adjustedPrice", i, type)}
          />
        );
      }
    } else {
      console.log("bo");
      return (
        <TextField
          style={{
            margin: "0px",
          }}
          autoComplete="off"
          id="standard-number"
          type="number"
          disabled
          margin="normal"
          value={input.price}
          onChange={this.props.changeInputValue("adjustedPrice", i, type)}
        />
      );
    }
  };

  render() {
    console.log("test");
    const { config, type, title } = this.props,
      length = Object.keys(config[type]).length - 1;

    if (config.currentProd.sku) {
      return (
        <div className="form-section addon-selectors">
          <h3> Select {title} </h3>
          <div className="label-flex">
            <div
              style={{
                width: "131px",
                marginRight: "10px",
              }}
            >
              Product
            </div>
            <div
              style={{
                width: "35px",
                marginRight: "10px",
              }}
            >
              Qty.
            </div>
            <div
              style={{
                width: "70px",
              }}
            >
              Price
            </div>
            <div />
          </div>
          {_.map(config[type], (input, i) => {
            return (
              <div key={this.id} className="add-container">
                <div
                  style={{
                    display: "table",
                    marginRight: "10px",
                  }}
                >
                  <select
                    style={{
                      width: "130px",
                    }}
                    value={input.sku}
                    onChange={this.props.handleAddonSelect(type, "value", i)}
                  >
                    <option value="Select Addon"> Select {title} </option>
                    {this.renderOptions(type)}
                  </select>
                </div>
                <div
                  style={{
                    width: "35px",
                    marginRight: "10px",
                  }}
                >
                  <TextField
                    style={{
                      margin: "0px",
                    }}
                    autoComplete="off"
                    id="standard-number"
                    type="number"
                    name={this.id}
                    margin="normal"
                    value={input.qty}
                    onChange={this.props.changeInputValue("qty", i, type)}
                  />
                </div>
                <div
                  style={{
                    width: "70px",
                  }}
                >
                  {this.renderPrice(input, type, i)}
                </div>
                {this.renderRemove(i, length, type)}
                {this.renderAdd(i, length, type)}
              </div>
            );
          })}
        </div>
      );
    } else {
      return false;
    }
  }
}

export default AddonField;
