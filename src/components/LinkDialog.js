import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import { CopyToClipboard } from "react-copy-to-clipboard";

export default class LinkDialog extends Component {
  state = {
    copied: false
  };

  handleFocus = event => event.target.select();

  render() {
    const url = window.location.href;
    return (
      <Dialog
        maxWidth={false}
        open={this.props.linkDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <div className="dialog-container">
          <h3>Share this project</h3>
          <div className="dialog-input">
            <input
              onFocus={this.handleFocus}
              value={url}
              onChange={({ target: { value } }) =>
                this.setState({ value, copied: false })
              }
            />

            <CopyToClipboard
              text={url}
              onCopy={() => this.setState({ copied: true })}>
              <i class="far fa-copy" />
            </CopyToClipboard>
            <div className="is-size-9">
              {this.state.copied ? "Copied to your clipboard" : ""}
            </div>
            <p>
              This link is read-only and allows outside users access to your
              quote
            </p>
          </div>
          <div className="columns">
            <div
              className="button is-primary"
              onClick={this.props.closeLinkDialog()}>
              Close
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}
