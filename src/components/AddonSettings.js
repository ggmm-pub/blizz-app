import React, { Component } from "react";
import _ from "lodash";
import TextField from "@material-ui/core/TextField";

import { fire } from "../firebase/firebase";

export default class AddonSettings extends Component {
  constructor() {
    super();
    this.state = {
      addons: [],
    };
  }
  handleChange = (name, i) => (e) => {
    this.setState({
      ...this.state,
      addons: {
        ...this.state.addons,
        [i]: {
          ...this.state.addons[i],
          [name]: e.target.value,
        },
      },
    });
  };

  appendAddon = (i) => {
    const uniqueId = +new Date();
    const newInput = {
        sku: "Enter SKU",
        price: 0,
        id: uniqueId,
      },
      index = parseInt(i, 10);

    this.setState((prevState) => ({
      ...this.state,
      addons: {
        ...this.state.addons,
        [index]: newInput,
      },
    }));
  };

  componentDidMount() {
    fire
      .database()
      .ref("/addons")
      .once("value")
      .then((snapshot) => {
        this.setState({
          addons: snapshot.val(),
        });
      });
  }

  removeCallback() {
    fire.database().ref("/addons").set(this.state.addons);
  }

  removeProduct = (index) => (e) => {
    const { addons } = this.state,
      removed = _.filter(addons, (x, i) => i !== index);

    this.setState(
      {
        addons: removed,
      },
      () => this.removeCallback()
    );
  };

  renderProducts() {
    const { addons } = this.state;

    if (addons) {
      return _.map(addons, (o, i) => {
        return (
          <div className="item-settings">
            <div className="row">
              <TextField
                autoComplete="off"
                margin="normal"
                label="SKU"
                value={o.sku}
                onChange={this.handleChange("sku", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Dealer Price"
                value={o.dealerPrice}
                onChange={this.handleChange("dealerPrice", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="MAP Price"
                value={o.price}
                onChange={this.handleChange("price", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Special Tag"
                value={o.tag}
                onChange={this.handleChange("tag", i)}
              />
              {o.weight && (
                <TextField
                  autoComplete="off"
                  margin="normal"
                  label="Weight (lbs)"
                  value={o.weight}
                  onChange={this.handleChange("weight", i)}
                />
              )}
            </div>

            <div className="row" style={{ width: "300px", marginTop: "30px" }}>
              <div
                className="btn blue"
                onClick={this.props.handleSaveProduct(
                  this.state.addons,
                  "addons"
                )}
              >
                Save
              </div>
              <div className="btn red" onClick={this.removeProduct(i)}>
                Delete
              </div>
            </div>
          </div>
        );
      });
    } else {
      return <h4>Add Product</h4>;
    }
  }

  renderAddButton() {
    const { addons } = this.state;
    if (addons) {
      const i = Object.keys(addons).length;
      return (
        <div className="btn blue" onClick={() => this.appendAddon(i)}>
          Add New Addon
        </div>
      );
    } else {
      return (
        <div className="btn blue" onClick={() => this.appendAddon(0)}>
          Create Addon
        </div>
      );
    }
  }

  render() {
    return (
      <div className="product-settings">
        <h2>Addons</h2>
        <p>Add and remove addons from the calculator</p>
        {this.renderProducts()}
        {this.renderAddButton()}
      </div>
    );
  }
}
