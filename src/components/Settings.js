import React, { Component } from "react";

import ProductSettings from "./ProductSettings";
import AddonSettings from "./AddonSettings";
import ControllerSettings from "./ControllerSettings";

export default class Settings extends Component {
  constructor() {
    super();
    this.state = {
      panel: "products"
    };
  }

  renderView() {
    const { panel } = this.state,
      { handleSaveProduct, products } = this.props;
    if (panel === "overview") {
      return <h1>Overview</h1>;
    } else if (panel === "processors") {
      return <ControllerSettings handleSaveProduct={handleSaveProduct} />;
    } else if (panel === "addons") {
      return <AddonSettings handleSaveProduct={handleSaveProduct} />;
    } else if (panel === "products") {
      return (
        <ProductSettings
          handleSaveProduct={handleSaveProduct}
          products={products}
        />
      );
    }
  }

  handlePanel = name => e => {
    this.setState({
      panel: name
    });
  };
  renderSettings() {
    const { admin } = this.props,
      { panel } = this.state;

    if (admin) {
      return (
        <div>
          <div className="launch-pad">
            {/* <div
              onClick={this.handlePanel("defaults")}
              className={panel === "defaults" ? "active" : ""}
              >
              <i className="far fa-sliders-h" /> Defaults
            </div> */}
            <div
              onClick={this.handlePanel("products")}
              className={panel === "products" ? "active" : ""}
            >
              <i className="fas fa-bolt" /> Products
            </div>
            <div
              onClick={this.handlePanel("addons")}
              className={panel === "addons" ? "active" : ""}
            >
              <i className="far fa-plus-hexagon" /> Addons
            </div>
            <div
              onClick={this.handlePanel("processors")}
              className={panel === "processors" ? "active" : ""}
            >
              <i className="fas fa-asterisk" /> Processors
            </div>
          </div>
          <div className="flex-container">
            <div className="left" />
            <div className="launch-view">
              <div className="container">{this.renderView()}</div>
            </div>
          </div>
        </div>
      );
    }
  }
  render() {
    return <div className="settings">{this.renderSettings()}</div>;
  }
}
