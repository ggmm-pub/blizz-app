import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import axios from "axios";

export default class Form extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      fname: "",
      lname: "",
      message: ""
    };
  }
  handleSubmit = e => {
    const form = this.state;
    e.preventDefault();
    axios
      .post("/sendmail", {
        email_address: form.email,
        fname: form.fname,
        lname: form.lname,
        message: form.message
      })
      .catch(function(response) {
        console.log("error");
      });
  };
  render() {
    return (
      <div className="form align-center">
        <form
          onSubmit={this.handleSubmit}
          style={{ padding: "50px", background: "white" }}
        >
          <div className="columns">
            <div className="column">
              <TextField
                id="with-placeholder"
                label="First Name"
                placeholder="First Name"
                margin="normal"
                onChange={e => this.setState({ fname: e.target.value })}
              />
            </div>
            <div className="column">
              <TextField
                id="with-placeholder"
                label="Last Name"
                placeholder="Last Name"
                margin="normal"
                onChange={e => this.setState({ lname: e.target.value })}
              />
            </div>
          </div>
          <TextField
            id="with-placeholder"
            label="Your Email"
            placeholder="Your Email"
            margin="normal"
            fullWidth
            onChange={e => this.setState({ email: e.target.value })}
            style={{ marginBottom: "30px" }}
          />
          <TextField
            id="textarea"
            label="Your Message"
            placeholder="Your Message"
            multiline
            fullWidth
            onChange={e => this.setState({ message: e.target.value })}
            margin="normal"
            style={{ marginBottom: "50px" }}
          />
          <input type="submit" className="button is-primary is-fullwidth" />
        </form>
      </div>
    );
  }
}
