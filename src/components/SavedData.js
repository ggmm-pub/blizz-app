import React, { Component } from "react";
import _ from "lodash";
import { Link } from "react-router-dom";

export default class SavedData extends Component {
  renderSaves() {
    if (this.props.saves) {
      return _.map(this.props.saves, saves => {
        return (
          <div
            className="save-database"
            key={saves.id}
            style={{ marginBottom: "20px" }}
          >
            <div className="flex-container">
              <div className="save-box">
                <div className="save-file">{saves.saveName}</div>
                <div className="save-ui">
                  <Link
                    className="button is-primary"
                    style={{ marginRight: "10px" }}
                    to={"/editor/" + this.props.uid + "/" + saves.id}
                  >
                    Load
                  </Link>
                  <div
                    className="delete"
                    onClick={() => {
                      this.props.remove(saves.id);
                    }}
                  >
                    <i className="far fa-trash-alt" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      return (
        <div>
          <h3>You have no saved quotes</h3>
          <Link to="/editor" className="button">
            Create One
          </Link>
        </div>
      );
    }
  }

  render() {
    return (
      <div
        className="container"
        style={{ paddingTop: "150px", minHeight: "100vh" }}
      >
        {this.renderSaves()}
      </div>
    );
  }
}
