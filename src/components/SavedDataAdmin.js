import React, { Component } from "react";
import _ from "lodash";
import { Link } from "react-router-dom";
import Search from "./Search";
import { fire } from "../firebase/firebase";

export default class SavedDataAdmin extends Component {
  constructor() {
    super();
    this.state = {
      searchInput: "",
      savesLoaded: false,
      saves: []
    };
  }

  componentDidMount() {
    this.getAllSaves();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.isNewSave && !prevState.savesLoaded) {
      console.log("update");
      this.getAllSaves();
    }
  }

  getAllSaves() {
    fire
      .database()
      .ref("/users/")
      .orderByChild("hasSave")
      .equalTo(true)
      .once("value")
      .then(snapshot => {
        let data = [];
        // eslint-disable-next-line
        const map = _.map(snapshot.val(), allSaves => {
          return _.map(allSaves.savedData, savedData => {
            const title = savedData.saveName,
              company = allSaves.company;
            data.push({
              title: title,
              company: company,
              uid: savedData.uid,
              id: savedData.id
            });
          });
        });
        this.setState({
          saves: data
        });
      });
  }

  handleChange = event => {
    this.setState({
      searchInput: event.target.value
    });
  };

  renderSaveBar() {
    return (
      <Search
        handleChange={this.handleChange}
        search={this.state.searchInput}
      />
    );
  }

  renderSaves() {
    const saveEntry = this.state.saves;
    const search = this.state.searchInput.toLowerCase();
    let searchResults = _.filter(saveEntry, function(o) {
      let title = "",
        company = "";
      if (o.title !== undefined) {
        title = o.title.toLowerCase();
      }
      if (o.company !== undefined) {
        company = o.company.toLowerCase();
      }
      const data = title + company;

      return _.includes(data, search);
    });

    return _.map(_.reverse(searchResults), saves => {
      return (
        <div
          className={
            this.props.state[saves.id]
              ? "save-database fade-left"
              : "save-database"
          }
          key={saves.id}
          style={{ marginBottom: "20px" }}>
          <div className="flex-container">
            <div className="save-box">
              <div className="columns">
                <div className="column">
                  <div className="save-file">{saves.title}</div>
                </div>
                <div className="column">{saves.company}</div>
              </div>
              <div className="save-ui">
                <Link
                  className="button is-primary"
                  style={{ marginRight: "10px" }}
                  to={"/editor/" + this.props.uid + "/" + saves.id}>
                  Load
                </Link>
                <div
                  className="delete"
                  onClick={() => {
                    this.props.remove(saves.id);
                  }}>
                  <i className="far fa-trash-alt" />
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    return (
      <div
        className="container"
        style={{ paddingTop: "50px", minHeight: "100vh" }}>
        <div className="product-settings">
          <h2>Saves</h2>
          <p>View and search all saves and users.</p>
        </div>
        {this.renderSaveBar()}
        {Object.keys(this.props.saves).length !== 0 && (
          <div className="columns top-sort">
            <div className="column">Save Name</div>
            <div className="column">Company</div>
          </div>
        )}

        {this.renderSaves()}
      </div>
    );
  }
}
