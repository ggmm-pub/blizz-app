import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";

class SaveModal extends Component {
  render() {
    const { isSaveModalOpen, config } = this.props;
    const isInvalid = !config.yourName && !config.custName && !config.saveName;

    return (
      <Dialog
        open={isSaveModalOpen}
        onClose={this.props.closeDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <div className="form-section">
          <div className="input-styles">
            <div className="close" />
            <div className="save-modal-flex">
              <div>
                <h3>Enter Your Details to Save</h3>
              </div>
              <TextField
                id="yourName"
                label="Your Name"
                margin="normal"
                value={config.yourName}
                onChange={this.props.handleInputChange("yourName")}
              />

              <TextField
                id="custName"
                label="Customer Name"
                margin="normal"
                value={config.custName}
                onChange={this.props.handleInputChange("custName")}
              />

              <TextField
                id="discount"
                label="Give Project a Name"
                margin="normal"
                value={config.saveName}
                onChange={this.props.handleInputChange("saveName")}
              />
              <div className="save-modal-button">
                <button
                  disabled={isInvalid}
                  className="button is-primary"
                  style={{ width: "48%", marginTop: "20px" }}
                  onClick={this.props.saveForm()}>
                  Save
                </button>
                <button
                  style={{ width: "48%", marginTop: "20px" }}
                  onClick={this.props.closeDialog("isSaveModalOpen")}
                  className="button is-secondary">
                  Cancel
                </button>
              </div>
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}

export default SaveModal;
