import React, { Component } from "react";
import Form from "./Form";

export default class Blank extends Component {
  render() {
    return (
      <div className="hero is-fullheight">
        <Form />
      </div>
    );
  }
}
