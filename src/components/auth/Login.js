import React, { Component } from "react";
import AuthForm from "./AuthForm";
import iris from "../../assets/iris.svg";
export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      history: ""
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <section>
        <div className="login-container">
          <div style={{ width: "100%" }}>
            <img src={iris} alt="Iris" />
          </div>
          <AuthForm location={this.props.location} login={this.props.login} />
        </div>
      </section>
    );
  }
}
