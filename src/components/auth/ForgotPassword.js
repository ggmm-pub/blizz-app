import React, { useCallback, useState } from "react";
import { fire } from "../../firebase/";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const ForgotPassword = props => {
  const [errorMsg, setErrorMsg] = useState();
  const handleForgottenPassword = useCallback(async event => {
    event.preventDefault();
    const { email } = event.target.elements;
    try {
      await fire.auth().sendPasswordResetEmail(email.value);
      setErrorMsg(
        "An email with instructions to reset your password have been sent to the email you've entered."
      );
    } catch (error) {
      alert(error);
    }
  }, []);

  return (
    <div style={{ textAlign: "center" }}>
      <p
        className="underline"
        style={{ marginBottom: 20, cursor: "pointer" }}
        onClick={() => props.setFormMode(false)}
      >
        🠐 Back to Login
      </p>

      <h3>Forgot Your Password?</h3>
      <p>
        Enter your email address and we'll send you instructions on how to reset
        your password.
      </p>
      <form onSubmit={handleForgottenPassword}>
        <TextField
          style={{ width: "100%" }}
          className="input-full"
          id="outlined-basic"
          label="Email"
          margin="normal"
          variant="outlined"
          inputProps={{
            name: "email"
          }}
        />
        <Button
          style={{ width: "100%" }}
          type="submit"
          variant="contained"
          color="primary"
        >
          Send Email
        </Button>
        <p style={{ paddingTop: 20 }}>{errorMsg}</p>
      </form>
    </div>
  );
};

export default ForgotPassword;
