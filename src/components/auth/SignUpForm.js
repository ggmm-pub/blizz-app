import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import { fire } from "../../firebase/firebase";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import axios from "axios";
import { auth } from "../../firebase/";
import ReactGA from "react-ga";

const INITIAL_STATE = {
  isDistributor: false,
  distributorStatus: "End User",
  showPassword: false,
  checked: false,
  fname: "",
  lname: "",
  email: "",
  company: "",
  password: "",
  error: null,
  listMembers: "",
};

export default class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.checked });
  };

  handleDistributors = (name) => (event) => {
    if (this.state.isDistributor) {
      this.setState({
        [name]: event.target.checked,
        distributorStatus: "End User",
      });
    } else {
      this.setState({
        [name]: event.target.checked,
        distributorStatus: "Pending Dealer",
      });
    }
  };

  handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  handleClickShowPassword = () => {
    this.setState((state) => ({ showPassword: !state.showPassword }));
  };

  sendToHubspot = (obj, e) => {
    //google analytics
    ReactGA.event({
      category: "lead",
      action: "leadGen",
      label: "Lead Gen with Signup",
      value: distributorStatus, //end-user or pending dealer
    });

    const {
      email,
      password,
      lname,
      fname,
      company,
      distributorStatus,
    } = this.state;
    const { setAccount } = this.props;

    axios.post("https://api.blizzard.lighting/update-contact", obj).then(() => {
      auth
        .doCreateUserWithEmailAndPassword(email, password)
        .then((authUser) => {
          fire
            .database()
            .ref("users/" + authUser.user.uid)
            .set({
              company: company,
              email: email,
              distributorStatus: distributorStatus,
              displayName: fname + " " + lname,
              uid: authUser.user.uid,
            });
        })
        .catch((error) => {
          this.setState({
            error: error,
          });
        })
        .then(setAccount());
    });
  };

  onSubmit = (e) => {
    e.preventDefault();

    const { email, lname, fname, distributorStatus } = this.state;

    this.sendToHubspot(
      {
        email: email,
        firstname: fname,
        lastname: lname,
        iris_calculator_user: distributorStatus,
      },
      e
    );
  };

  render() {
    const { name, email, password, error, distributorStatus } = this.state;
    console.log(distributorStatus);

    const isInvalid =
      password === "" || password.length < 6 || email === "" || name === "";

    return (
      <form onSubmit={this.onSubmit}>
        <TextField
          style={{ marginRight: "5%", width: "45%" }}
          id="lname"
          name="FNAME"
          label="First Name"
          placeholder="First Name"
          margin="normal"
          onChange={(e) => this.setState({ fname: e.target.value })}
        />
        <TextField
          style={{ width: "50%" }}
          id="fname"
          name="LNAME"
          label="Last Name"
          placeholder="Last Name"
          margin="normal"
          onChange={(e) => this.setState({ lname: e.target.value })}
        />
        <FormControl fullWidth>
          <TextField
            id="email"
            name="EMAIL"
            label="Email"
            placeholder="Enter Your Email"
            margin="normal"
            onChange={(e) => this.setState({ email: e.target.value })}
          />

          <TextField
            id="company"
            name="COMPANY"
            label="Company"
            placeholder="Comany Name"
            margin="normal"
            onChange={(e) => this.setState({ company: e.target.value })}
          />

          <FormControlLabel
            control={
              <Checkbox
                checked={this.state.isDistributor}
                onChange={this.handleDistributors("isDistributor")}
                value="checkedB"
                color="primary"
              />
            }
            label="Apply for Reseller Access"
          />

          <FormControl style={{ marginTop: "20px" }}>
            <InputLabel htmlFor="adornment-password">Password</InputLabel>
            <Input
              id="adornment-password"
              placeholder="Enter a password"
              type={this.state.showPassword ? "text" : "password"}
              value={this.state.password}
              onChange={(e) => this.setState({ password: e.target.value })}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.handleClickShowPassword}
                    onMouseDown={this.handleMouseDownPassword}
                  >
                    {this.state.showPassword ? (
                      <VisibilityOff />
                    ) : (
                      <Visibility />
                    )}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>

          <div className="columns" style={{ paddingTop: "30px" }}>
            <div className="column is-fullwidth">
              <div className="error-message">{this.state.error}</div>
              <button
                disabled={isInvalid}
                className="button is-primary is-fullwidth"
                type="submit"
              >
                Sign Up
              </button>
              {error && <p className="form-error">{error.message}</p>}
            </div>
          </div>
        </FormControl>
      </form>
    );
  }
}
