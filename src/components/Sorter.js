import React from "react";

const Sorter = (props) => {
  const { data, sortBy, callback, sorted } = props;

  const sortData = async () => {
    console.log(data);
    if (data) {
      const sortedData = await _.orderBy(data, [sortBy], [sorted]);

      callback(sortedData);
    }
  };
  return (
    <div className="sorter" onClick={() => sortData()}>
      <i class="fas fa-sort"></i>
    </div>
  );
};

export default Sorter;
