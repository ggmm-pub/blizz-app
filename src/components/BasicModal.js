import React, { Component } from "react";
import EmailForm from "./EmailForm";

export default class BasicModal extends Component {
  render() {
    if (!this.props.modalState) {
      return null;
    }
    return (
      <div className="modal is-active">
        <div className="modal-background" />
        <div className="modal-content">
          <div>
            <div className="box">
              <EmailForm />
            </div>
          </div>
        </div>
        <button
          className="modal-close is-large"
          aria-label="close"
          onClick={this.props.closeModal}
        />
      </div>
    );
  }
}
