import React, { Component } from "react";

export default class QuoteRow extends Component {
  render() {
    return (
      <div className="columns csv-prod">
        <div className="column csv-qty">{this.props.qty}</div>
        <div className="column csv-product">{this.props.title}</div>
        <div className="column csv-unit">${this.props.pricePer}</div>
        <div className="column subtotal csv-sub">
          ${this.props.total.toFixed(2)}
        </div>
      </div>
    );
  }
}
