import React, { Component } from "react";
import _ from "lodash";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";

import { fire } from "../firebase/firebase";

export default class ProductSettings extends Component {
  constructor() {
    super();
    this.state = {
      products: []
    };
  }
  handleChange = (name, i) => e => {
    this.setState({
      ...this.state,
      products: {
        ...this.state.products,
        [i]: {
          ...this.state.products[i],
          [name]: e.target.value
        }
      }
    });
  };

  appendProduct = i => {
    const uniqueId = +new Date();
    const newInput = {
        productName: "",
        sku: "",
        width: "",
        height: "",
        weight: "",
        pixelWidth: "",
        pixelHeight: "",
        pixelsPerPanel: "",
        casePak: "",
        powerConsumption: "",
        price: "",
        id: uniqueId
      },
      index = parseInt(i, 10);

    this.setState(prevState => ({
      ...this.state,
      products: {
        ...this.state.products,
        [index]: newInput
      }
    }));
  };

  removeCallback() {
    fire
      .database()
      .ref("/products")
      .set(this.state.products);
  }

  removeProduct = index => e => {
    const { products } = this.state,
      removed = _.filter(products, (x, i) => i !== index);

    this.setState(
      {
        products: removed
      },
      () => this.removeCallback()
    );
  };

  componentDidMount() {
    fire
      .database()
      .ref("/products")
      .once("value")
      .then(snapshot => {
        this.setState({
          products: snapshot.val()
        });
      });
  }
  renderProducts() {
    const { products } = this.state;
    if (products) {
      return _.map(products, (o, i) => {
        return (
          <div className="item-settings">
            <div className="row">
              <TextField
                autoComplete="off"
                margin="normal"
                label="Product Name"
                value={o.productName}
                onChange={this.handleChange("productName", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Dealer Price"
                value={o.dealerPrice}
                onChange={this.handleChange("dealerPrice", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="MAP Price"
                value={o.price}
                onChange={this.handleChange("price", i)}
              />
            </div>
            <div className="row">
              <TextField
                autoComplete="off"
                margin="normal"
                label="Pixel Width"
                value={o.pixelWidth}
                onChange={this.handleChange("pixelWidth", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Pixel Height"
                value={o.pixelHeight}
                onChange={this.handleChange("pixelHeight", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Pixels Per Panel"
                value={o.pixelsPerPanel}
                onChange={this.handleChange("pixelsPerPanel", i)}
              />
            </div>
            <div className="row">
              <TextField
                autoComplete="off"
                margin="normal"
                label="Width"
                value={o.width}
                onChange={this.handleChange("width", i)}
                InputProps={{
                  endAdornment: (
                    <InputAdornment variant="filled" position="end">
                      mm
                    </InputAdornment>
                  )
                }}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Height"
                value={o.height}
                onChange={this.handleChange("height", i)}
                InputProps={{
                  endAdornment: (
                    <InputAdornment variant="filled" position="end">
                      mm
                    </InputAdornment>
                  )
                }}
              />

              <TextField
                autoComplete="off"
                margin="normal"
                label="Weight"
                value={o.weight}
                onChange={this.handleChange("weight", i)}
                InputProps={{
                  endAdornment: (
                    <InputAdornment variant="filled" position="end">
                      lbs.
                    </InputAdornment>
                  )
                }}
              />
            </div>
            <div className="row">
              <TextField
                autoComplete="off"
                margin="normal"
                label="Case Pak"
                value={o.casePak}
                onChange={this.handleChange("casePak", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Sku"
                value={o.sku}
                onChange={this.handleChange("sku", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Power Consumption"
                value={o.powerConsumption}
                onChange={this.handleChange("powerConsumption", i)}
              />
            </div>
            <div className="row" style={{ width: "300px", marginTop: "30px" }}>
              <div
                className="btn blue"
                onClick={this.props.handleSaveProduct(
                  this.state.products,
                  "products"
                )}
              >
                Save
              </div>
              <div className="btn red" onClick={this.removeProduct(i)}>
                Delete
              </div>
            </div>
          </div>
        );
      });
    } else {
      return <h4>Add Product</h4>;
    }
  }

  renderAddButton() {
    const { products } = this.state;
    if (products) {
      const i = Object.keys(products).length;

      return (
        <div className="btn blue" onClick={() => this.appendProduct(i)}>
          Add New Product
        </div>
      );
    } else {
      return (
        <div className="btn blue" onClick={() => this.appendProduct(0)}>
          Create Product
        </div>
      );
    }
  }
  render() {
    // const i = Object.keys(this.props.products).length;

    return (
      <div className="product-settings">
        <h2>Current Products</h2>
        <p>Add and remove products from the calculator</p>
        {this.renderProducts()}

        {this.renderAddButton()}
      </div>
    );
  }
}
