import React, { Component } from "react";
import _ from "lodash";
import TextField from "@material-ui/core/TextField";

import { fire } from "../firebase/firebase";

export default class ControllerSettings extends Component {
  constructor() {
    super();
    this.state = {
      processors: []
    };
  }
  handleChange = (name, i) => e => {
    this.setState({
      ...this.state,
      processors: {
        ...this.state.processors,
        [i]: {
          ...this.state.processors[i],
          [name]: e.target.value
        }
      }
    });
  };

  appendAddon = i => {
    const uniqueId = +new Date();
    const newInput = {
        sku: "Enter SKU",
        price: 0,
        id: uniqueId
      },
      index = parseInt(i, 10);

    this.setState(prevState => ({
      ...this.state,
      processors: {
        ...this.state.processors,
        [index]: newInput
      }
    }));
  };

  componentDidMount() {
    fire
      .database()
      .ref("/processors")
      .once("value")
      .then(snapshot => {
        this.setState({
          processors: snapshot.val()
        });
      });
  }

  removeCallback() {
    fire
      .database()
      .ref("/processors")
      .set(this.state.processors);
  }

  removeProduct = index => e => {
    const { processors } = this.state,
      removed = _.filter(processors, (x, i) => i !== index);

    this.setState(
      {
        processors: removed
      },
      () => this.removeCallback()
    );
  };

  renderProducts() {
    const { processors } = this.state;
    if (processors) {
      return _.map(processors, (o, i) => {
        return (
          <div className="item-settings">
            <div className="row">
              <TextField
                autoComplete="off"
                margin="normal"
                label="Name"
                value={o.sku}
                onChange={this.handleChange("sku", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Part Number"
                value={o.partNum}
                onChange={this.handleChange("partNum", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Dealer Price"
                value={o.dealerPrice}
                onChange={this.handleChange("dealerPrice", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="MAP Price"
                value={o.price}
                onChange={this.handleChange("price", i)}
              />
            </div>

            <div className="row">
              <TextField
                autoComplete="off"
                margin="normal"
                label="Output Ports"
                value={o.outputPorts}
                onChange={this.handleChange("outputPorts", i)}
              />
              <TextField
                autoComplete="off"
                margin="normal"
                label="Max Pixels"
                value={o.maxPixels}
                onChange={this.handleChange("maxPixels", i)}
              />
            </div>

            <div className="row" style={{ width: "300px", marginTop: "30px" }}>
              <div
                className="btn blue"
                onClick={this.props.handleSaveProduct(
                  this.state.processors,
                  "processors"
                )}
              >
                Save
              </div>
              <div className="btn red" onClick={this.removeProduct(i)}>
                Delete
              </div>
            </div>
          </div>
        );
      });
    } else {
      return <h4>Add Processor</h4>;
    }
  }

  renderAddButton() {
    const { processors } = this.state;
    if (processors) {
      const i = Object.keys(processors).length;
      return (
        <div className="btn blue" onClick={() => this.appendAddon(i)}>
          Add New Processor
        </div>
      );
    } else {
      return (
        <div className="btn blue" onClick={() => this.appendAddon(0)}>
          Create Processor
        </div>
      );
    }
  }

  render() {
    return (
      <div className="product-settings">
        <h2>Processors</h2>
        <p>Add and remove processors from the calculator</p>
        {this.renderProducts()}
        {this.renderAddButton()}
      </div>
    );
  }
}
