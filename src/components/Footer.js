import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <footer
        id="footer"
        role="contentinfo"
        itemscope
        itemtype="http://schema.org/WPFooter"
      >
        <p>GoGeddit Marketing and Media © {new Date().getFullYear()}</p>
      </footer>
    );
  }
}
