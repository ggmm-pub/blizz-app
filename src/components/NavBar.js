import React, { Component } from "react";
import { Link } from "react-router-dom";
import logo from "../assets/blizzard.svg";

export default class NavBar extends Component {
  renderSaveRoute() {
    if (this.props.admin) {
      return (
        <Link to="/admin" className="navbar-item">
          Saves
        </Link>
      );
    } else {
      return (
        <Link to="/saves" className="navbar-item">
          Saves
        </Link>
      );
    }
  }

  renderSettings() {
    if (this.props.admin) {
      return (
        <Link className="navbar-item" to="/settings">
          <i className="far fa-cog" />
        </Link>
      );
    }
  }

  renderDistributor() {
    if (this.props.admin) {
      return (
        <Link className="navbar-item" to="/dealers">
          Users
        </Link>
      );
    }
  }

  accountType() {
    const { distributorStatus, admin } = this.props;
    if (distributorStatus === "Dealer" && !admin) {
      return "Reseller";
    } else if (admin) {
      return "Admin";
    } else if (distributorStatus === "pending") {
      return "Distributor (Pending)";
    } else {
      return "Consumer";
    }
  }

  render() {
    return (
      <div className="header">
        <link rel="stylesheet" href="https://use.typekit.net/ime6mgh.css" />
        <div className="branding">
          <Link to="/editor">
            <img className="logo" src={logo} alt="Blizzard pro" />
          </Link>
          <div className="account-type">{this.accountType()}</div>
        </div>
        <section className="login">
          {this.props.user ? (
            <div style={{ display: "flex" }}>
              <Link to="/editor" className="navbar-item">
                Editor
              </Link>
              {this.renderSaveRoute()}
              {this.renderDistributor()}

              <div
                style={{ cursor: "pointer" }}
                className="navbar-item"
                onClick={this.props.logout}
              >
                Logout
              </div>
              {this.renderSettings()}
            </div>
          ) : (
            <div className="flex-container">
              <Link to="/editor" className="navbar-item">
                Editor
              </Link>
              <Link to="/login" className="navbar-item">
                Login
              </Link>
            </div>
          )}
        </section>
      </div>
    );
  }
}
