import React, { Component } from "react";

import Calculator from "./Calculator";
import CustomerView from "./CustomerView";
import SaveDialog from "./SaveDialog";
import LinkDialog from "./LinkDialog";
import SaveModal from "./SaveModal";
import axios from "axios";
import { fire } from "../firebase/firebase";
import { auth } from "../firebase/";

import {
  Tooltip,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Checkbox,
  Radio,
  Button,
} from "@material-ui/core";
import { matchPath } from "react-router";
import { Link } from "react-router-dom";
import Cookies from "universal-cookie";
import { CSVLink } from "react-csv";
import ReactGA from "react-ga";

import _ from "lodash";
const cookies = new Cookies();

class Home extends Component {
  state = {
    snackbar: false,
    isSaveModalOpen: false,
    viewBar: true,
    linkDialog: false,
    welcome: true,
    leadGen: false,
    formActive: false,
    csvData: [],
    createAccount: true,
    timeline: "",
    resellerAccess: false,
    password: "",
    email: "",
    firstName: "",
    lastName: "",
    confirmPw: "",
    company: "",
    showErrors: false,
  };

  closeWelcome = () => (e) => {
    ReactGA.event({
      category: "lead",
      action: "formFill",
      label: "Form Filled",
    });
    this.setState({
      formActive: false,
    });
  };

  componentDidMount() {
    if (this.props.history.location.pathname != null) {
      const match = matchPath(this.props.history.location.pathname, {
        path: "/editor/:uid/:saveid",
        exact: false,
        strict: false,
      });
      if (match !== null) {
        const params = match.params;
        this.props.readOnlyLink(params.uid, params.saveid);
      }
    }
  }

  fetchCSVData() {
    const { config } = this.props;

    const csvData = [];

    //Product
    const totalPanels = +config.totalPanelsNeeded + +config.completePanels;
    const product = {
      qty: totalPanels,
      sku: config.currentProd.sku,
      unitPrice: config.currentProd.dealerPrice,
      subtotal: config.currentProd.dealerPrice * totalPanels,
    };
    //Cases
    const cases = {
      qty: config.caseNeeded,
      sku: "CASE - IRIS6",
      unitPrice: 299.99,
      subtotal: config.caseNeeded * 299.99,
    };
    //Bumpers
    const singleBumpers = {
      qty: config.singleBumpers,
      sku: "IRIS - FLYBAR-1",
      unitPrice: 254.99,
      subtotal: config.singleBumpers * 254.99,
    };
    const dualBumpers = {
      qty: config.dualBumpers,
      sku: "IRIS - FLYBAR-2",
      unitPrice: 324.99,
      subtotal: config.dualBumpers * 324.99,
    };
    //Processors

    let processors = [],
      processorsTotal = 0,
      // eslint-disable-next-line
      processorSum = _.map(config.processors, (processor) => {
        const processorSubTotal = +processor.qty * processor.dealerPrice;
        processorsTotal = +processorSubTotal + +processorsTotal;
        const data = {
          qty: processor.qty,
          sku: processor.sku,
          unitPrice: processor.dealerPrice,
          subtotal: processorSubTotal,
        };
        processors.push(data);
      });

    let addons = [],
      addonsTotal = 0,
      // eslint-disable-next-line
      addonSum = _.map(config.addons, (addon) => {
        if (addon.sku !== "None") {
          const addonSubTotal = +addon.qty * addon.dealerPrice;
          addonsTotal = +addonSubTotal + +addonsTotal;
          const data = {
            qty: addon.qty,
            sku: addon.sku,
            unitPrice: addon.dealerPrice,
            subtotal: addonSubTotal,
          };
          addons.push(data);
        }
      });

    const total =
      +product.subtotal +
      +cases.subtotal +
      +singleBumpers.subtotal +
      +dualBumpers.subtotal +
      +processorsTotal +
      +addonsTotal;
    const totaler = {
        qty: "",
        sku: "",
        unitPrice: "Total",
        subtotal: total + "*",
      },
      fineprint = {
        qty: "",
        sku: "",
        unitPrice: "",
        subtotal: "*Quote is an estimate. Does not include tax.",
      };

    csvData.push(
      product,
      cases,
      singleBumpers,
      dualBumpers,
      ...processors,
      ...addons,
      totaler,
      fineprint
    );

    this.setState({
      csvData: csvData,
    });
  }

  renderLinkIcon() {
    if (this.props.config.saveLoad && this.props.user) {
      return (
        <Tooltip title="Shareable Link">
          <i
            onClick={() => this.setState({ linkDialog: true })}
            className="far fa-link"
          />
        </Tooltip>
      );
    }
  }

  closeLinkDialog = () => (e) => {
    this.setState({
      linkDialog: false,
    });
  };
  renderVisibility = () => {
    if (this.state.viewBar) {
      return (
        <Tooltip placement="bottom" title="Hide Bar">
          <i
            onClick={() => this.setState({ viewBar: false })}
            className="far fa-eye-slash"
          />
        </Tooltip>
      );
    } else {
      return (
        <Tooltip placement="bottom" title="Show Bar">
          <i
            onClick={() => this.setState({ viewBar: true })}
            className="far fa-eye"
          />
        </Tooltip>
      );
    }
  };

  handleSignUp = () => {
    const success = () => {
      cookies.set("hideWelcome", "true");
      this.setState({
        leadGen: false,
        welcome: false,
        formActive: false,
      });
    };
    const {
      email,
      firstName,
      lastName,
      company,
      password,
      timeline,
    } = this.state;
    const hubspotData = {
      email: this.state.email,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      iris_calculator_user: this.state.resellerAccess
        ? "Pending Dealer"
        : "End User",
    };

    if (
      email.length > 0 &&
      firstName.length > 0 &&
      lastName.length > 0 &&
      company.length > 0 &&
      password.length > 0 &&
      timeline.length > 0
    ) {
      if (this.state.createAccount) {
        axios
          .post("https://api.blizzard.lighting/update-contact", hubspotData)
          .then(() => {
            auth
              .doCreateUserWithEmailAndPassword(
                this.state.email,
                this.state.password
              )
              .then((authUser) => {
                fire
                  .database()
                  .ref("users/" + authUser.user.uid)
                  .set({
                    company: this.state.company,
                    email: this.state.email,
                    distributorStatus: this.state.resellerAccess
                      ? "Pending Dealer"
                      : "End User",
                    displayName:
                      this.state.firstName + " " + this.state.lastName,
                    uid: authUser.user.uid,
                  });
              })
              .catch((error) => {
                this.setState({
                  error: error,
                });
              })
              .then(() => success());
          });
      } else {
        axios
          .post("https://api.blizzard.lighting/update-contact", hubspotData)
          .then(() => success())
          .catch((err) => console.log(err));
      }
    } else {
      this.setState({ showErrors: true });
    }
  };

  renderCalc() {
    const props = this.props;
    const { admin } = this.props;
    const {
      welcome,
      leadGen,
      formActive,
      password,
      email,
      firstName,
      lastName,
      confirmPw,
      resellerAccess,
      timeline,
      showErrors,
      company,
    } = this.state;
    const validatePw = password.length > 4;

    const showMessage = welcome;
    const cookieFilter = cookies.get("hideWelcome");
    const hideMessage = cookieFilter === "true";

    let savesPath = "/saves";

    if (admin) {
      savesPath = "/dealers";
    }

    if (this.props.user || !this.props.user) {
      return (
        <div className="calc">
          {showMessage && !hideMessage && !this.props.user && (
            <div className="welcome-modal">
              <div className="dialog-content">
                <div className="main-content">
                  <div className="container">
                    <h1>
                      Welcome to the IRiS <br /> LED Video Wall Builder
                    </h1>
                    <h3>What would you like to do today?</h3>
                    <div className="btn-group">
                      <a
                        onClick={() =>
                          this.setState({
                            leadGen: true,
                            welcome: false,
                            formActive: true,
                          })
                        }
                        className="btn blue"
                      >
                        Start New Project
                      </a>
                      <Link to="/login" className="btn otl">
                        Login
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

          {leadGen && (
            <div className="welcome-modal">
              <div className="dialog-content">
                <div className="main-content">
                  <div className="container">
                    <h1>
                      {!formActive
                        ? "Thank You"
                        : "Let us know a little about you"}
                    </h1>
                    {!formActive && (
                      <a
                        onClick={() => {
                          cookies.set("hideWelcome", "true");
                          this.setState({
                            leadGen: false,
                            welcome: false,
                            formActive: false,
                          });
                        }}
                        className="btn blue"
                        style={{
                          margin: "0 auto",
                          marginTop: 20,
                          display: "table",
                        }}
                      >
                        Continue
                      </a>
                    )}
                    <div className="form-group">
                      <TextField
                        error={
                          showErrors && firstName.length <= 1 ? true : false
                        }
                        style={{ width: "48%" }}
                        id="outlined-basic"
                        label="First Name"
                        margin="normal"
                        variant="outlined"
                        onChange={(e) =>
                          this.setState({ firstName: e.target.value })
                        }
                      />

                      <TextField
                        error={
                          showErrors && lastName.length <= 1 ? true : false
                        }
                        style={{ width: "48%" }}
                        id="outlined-basic"
                        label="Last Name"
                        margin="normal"
                        variant="outlined"
                        onChange={(e) =>
                          this.setState({ lastName: e.target.value })
                        }
                      />
                    </div>
                    <TextField
                      style={{ width: "100%" }}
                      id="outlined-basic"
                      label="Email"
                      margin="normal"
                      variant="outlined"
                      error={showErrors && email.length <= 1 ? true : false}
                      onChange={(e) => this.setState({ email: e.target.value })}
                    />
                    <TextField
                      style={{ width: "100%" }}
                      error={showErrors && company.length <= 1 ? true : false}
                      id="outlined-basic"
                      label="Company"
                      margin="normal"
                      variant="outlined"
                      onChange={(e) =>
                        this.setState({ company: e.target.value })
                      }
                    />

                    <FormControl
                      component="fieldset"
                      error={showErrors && timeline.length <= 1 ? true : false}
                      style={{
                        textAlign: "left",
                        display: "inherit",
                        marginTop: 25,
                      }}
                    >
                      <FormLabel component="legend">
                        Est. Timeline to Next Project:
                      </FormLabel>
                      <RadioGroup
                        row
                        aria-label="Est Timeline"
                        name="Est Timeline"
                        value={this.state.timeline}
                        onChange={(e) =>
                          this.setState({ timeline: e.target.value })
                        }
                      >
                        <FormControlLabel
                          value="female"
                          control={<Radio />}
                          label="0-3 Months"
                        />
                        <FormControlLabel
                          value="male"
                          control={<Radio />}
                          label="3-6 Months"
                        />
                        <FormControlLabel
                          value="other"
                          control={<Radio />}
                          label="6+ Months"
                        />
                      </RadioGroup>
                    </FormControl>

                    <FormControlLabel
                      style={{ width: "100%", paddingTop: 10 }}
                      control={
                        <Checkbox
                          checked={this.state.resellerAccess}
                          onChange={() =>
                            this.setState({
                              resellerAccess: !this.state.resellerAccess,
                            })
                          }
                          name="resellerAccess"
                          color="primary"
                        />
                      }
                      label="Request Reseller Access"
                    />

                    <FormControlLabel
                      style={{ width: "100%", paddingTop: 10 }}
                      control={
                        <Checkbox
                          checked={this.state.createAccount}
                          onChange={() =>
                            this.setState({
                              createAccount: !this.state.createAccount,
                            })
                          }
                          name="createAccount"
                          color="primary"
                        />
                      }
                      label="Create an Account"
                    />
                    {this.state.createAccount && (
                      <>
                        <TextField
                          error={validatePw ? false : true}
                          style={{ width: "100%" }}
                          id="outlined-basic"
                          label="Create Password"
                          margin="normal"
                          variant="outlined"
                          type="password"
                          value={password}
                          onChange={(e) =>
                            this.setState({ password: e.target.value })
                          }
                        />

                        <TextField
                          error={confirmPw === password ? false : true}
                          style={{ width: "100%" }}
                          id="outlined-basic"
                          label="Confirm Password"
                          margin="normal"
                          type="password"
                          variant="outlined"
                          onChange={(e) =>
                            this.setState({ confirmPw: e.target.value })
                          }
                          value={confirmPw}
                        />
                      </>
                    )}

                    <Button
                      style={{ width: "100%", marginTop: 20 }}
                      type="submit"
                      variant="contained"
                      color="primary"
                      onClick={() => this.handleSignUp()}
                      className="btn blue"
                    >
                      Submit
                    </Button>

                    <a
                      style={{ display: "table", margin: "0 auto" }}
                      onClick={() =>
                        this.setState({ leadGen: false, welcome: true })
                      }
                    >
                      Back
                    </a>
                  </div>
                </div>
              </div>
            </div>
          )}
          <Calculator
            user={this.props.user}
            changeProduct={this.props.changeProduct}
            removeAddon={this.props.removeAddon}
            addons={this.props.addons}
            processors={this.props.processors}
            handleRadio={this.props.handleRadio}
            openDialog={props.openDialog}
            admin={this.props.admin}
            distributorStatus={this.props.distributorStatus}
            handleTab={props.handleTab}
            handleAddonSelect={this.props.handleAddonSelect}
            changeInputValue={this.props.changeInputValue}
            appendInput={this.props.appendInput}
            handleQty={this.props.handleQty}
            changePrice={props.changePrice}
            item={props.item}
            config={props.config}
            saveLoad={props.saveLoad}
            clearState={props.clearState}
            handleChecked={props.handleChecked}
            handleInputChange={props.handleInputChange}
            saveForm={props.saveForm()}
            handleDimSelect={props.handleDimSelect}
            handleSelect={props.handleSelect}
          />
        </div>
      );
    }
  }

  renderStartCard = () => {
    if (this.props.config && this.props.config.saveLoad !== true) {
      const config = this.props.config,
        startMode =
          config.currentProd.sku === undefined && config.saveLoad !== true;
      if (startMode) {
        return (
          <div className="start-mode dialog">
            <h3>Start Here</h3>
            <p>Start by selecting a product</p>
          </div>
        );
      } else if (this.props.dimMode) {
        return (
          <div className="dim-mode dialog">
            <h3>Enter Dimensions</h3>
            <p>
              Pick from any two starting dimensions. We'll calculate the rest.
            </p>
          </div>
        );
      }
    }
  };

  render() {
    const {
        admin,
        distributorStatus,
        closeDialog,
        saveModal,
        saveDialog,
        isSaveModalOpen,
        resetForm,
        config,
        user,
        openDialog,
        handleTab,
        handleInputChange,
        videoEnabled,
        openedTab,
        price,
        saveForm,
        item,
        handleQuote,
      } = this.props,
      headers = [
        { label: "Qty", key: "qty" },
        { label: "Sku", key: "sku" },
        { label: "Unit Cost", key: "unitPrice" },
        { label: "Subtotal", key: "subtotal" },
      ],
      quoteAvailable = openedTab !== "quoteTab" && config.widthSelected,
      { csvData } = this.state;

    let csvFilename = "untitled-project";

    if (config.saveName) {
      csvFilename = config.saveName;
    }

    const dealerAccess = admin || distributorStatus === "Dealer";

    return (
      <div className="home">
        {this.renderStartCard()}
        <SaveModal
          handleInputChange={handleInputChange}
          config={config}
          saveModal={saveModal}
          isSaveModalOpen={isSaveModalOpen}
          closeDialog={closeDialog}
          saveForm={saveForm}
        />
        <SaveDialog
          saveDialog={saveDialog}
          openDialog={openDialog}
          closeDialog={closeDialog}
          resetForm={resetForm}
        />
        <LinkDialog
          linkDialog={this.state.linkDialog}
          closeLinkDialog={this.closeLinkDialog}
        />
        {this.renderCalc()}
        <div className="view">
          <div className="dash-bar">
            {quoteAvailable && (
              <div className="view-quote" onClick={handleTab("quoteTab")}>
                View Quote
              </div>
            )}

            <div className="dash-icons">
              {dealerAccess && (
                <CSVLink
                  filename={csvFilename + "-blizzard-videowall-quote.csv"}
                  data={csvData}
                  headers={headers}
                  onClick={() => this.fetchCSVData()}
                >
                  <i className="fas fa-file-csv" />
                </CSVLink>
              )}
              {this.renderLinkIcon()}
              {user && (
                <Tooltip title="Save">
                  <i
                    onClick={
                      config.saveName
                        ? this.props.saveForm()
                        : this.props.saveModal()
                    }
                    className="far fa-save"
                  />
                </Tooltip>
              )}

              {user && (
                <Tooltip title="Add Video">
                  <i
                    onClick={this.props.enableVideo()}
                    className="far fa-video"
                  />
                </Tooltip>
              )}

              {this.props.user && (
                <Tooltip title="New">
                  <i onClick={openDialog} className="far fa-file-alt" />
                </Tooltip>
              )}
              {this.renderVisibility()}
            </div>
          </div>

          <CustomerView
            handleQuote={handleQuote}
            distributorStatus={distributorStatus}
            admin={admin}
            viewBar={this.state.viewBar}
            handleTab={handleTab}
            openedTab={openedTab}
            videoEnabled={videoEnabled}
            item={item}
            config={config}
            price={price}
          />
        </div>
      </div>
    );
  }
}

export default Home;
