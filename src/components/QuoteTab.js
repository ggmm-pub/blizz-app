import React, { Component } from "react";
import _ from "lodash";
import QuoteRow from "./QuoteRow";

let prodTotal = 0,
  addonTotal = 0,
  dbTotal = 0,
  sbTotal = 0,
  caseTotal = 0,
  recCardTotal = 0;

export default class QuoteTab extends Component {
  numberWithCommas = (x) => {
    if (x !== undefined) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  };

  renderAddons = (type) => {
    const { config } = this.props;
    let addonSubTotal = 0,
      processorSubTotal = 0;

    if (
      this.props.admin ||
      (this.props.distributorStatus &&
        this.props.distributorStatus !== "pending")
    ) {
      // eslint-disable-next-line
      let addonSum = _.map(config.addons, (addon) => {
          const addonQty = addon.dealerPrice * addon.qty;
          addonSubTotal = +addonQty + +addonSubTotal;
          console.log(addon);
        }),
        //eslint-disable-next-line
        processorSum = _.map(config.processors, (processor) => {
          const processorQty = processor.dealerPrice * processor.qty;
          processorSubTotal = +processorQty + +processorSubTotal;
        });
    } else {
      // price = a.price;
      // priceQty = price * a.qty;
    }

    //eslint-disable-next-line
    addonTotal = addonSubTotal + processorSubTotal;

    return _.map(config[type], (a, i) => {
      if (a.name !== "None") {
        let price = 0;
        let priceQty = 0;

        if (
          this.props.admin ||
          (this.props.distributorStatus &&
            this.props.distributorStatus !== "pending")
        ) {
          price = a.dealerPrice;
          priceQty = price * a.qty;
        } else {
          price = a.price;
          priceQty = price * a.qty;
        }

        return (
          <QuoteRow
            key={i}
            qty={a.qty}
            title={a.sku}
            pricePer={price}
            total={priceQty}
          />
        );
      }
    });
  };

  renderProductRow() {
    if (this.props.config.totalPanelsNeeded) {
      const { config } = this.props,
        totalPanels =
          config.totalPanelsNeeded + parseInt(config.completePanels, 10);

      let price = 0;

      if (
        this.props.admin ||
        (this.props.distributorStatus &&
          this.props.distributorStatus !== "pending")
      ) {
        price = config.currentProd.dealerPrice;
        prodTotal = price * totalPanels;
      } else {
        price = config.currentProd.price;
        prodTotal = price * totalPanels;
      }

      return (
        <div className="columns csv-prod">
          <div className="column csv-qty">{totalPanels}</div>
          <div className="column csv-product">
            {config.currentProd.productName}
          </div>
          <div className="column csv-unit"> $ {price} </div>
          <div className="column subtotal csv-sub">
            $ {this.numberWithCommas(prodTotal.toFixed(2))}
          </div>
        </div>
      );
    }
  }

  // renderRecCard() {
  //   if (this.props.config.recCards > 0) {
  //     const config = this.props.config,
  //       beforeDisc = config.recCards * 39.99,
  //       beforeDiscount = beforeDisc.toFixed(2),
  //       disc = beforeDiscount * (config.discount / 100);
  //     recCardTotal = beforeDiscount - disc;

  //     return (
  //       <div className="columns">
  //         <div className="column"> {config.recCards} </div>
  //         <div className="column"> PART - IRIS - R3 - RECCARD </div>
  //         <div className="column"> $39 .99 </div>
  //         <div className="column subtotal">
  //           $ {this.numberWithCommas(recCardTotal.toFixed(2))}
  //         </div>
  //       </div>
  //     );
  //   }
  // }

  renderCases() {
    const { config } = this.props;

    let price = 0;

    if (
      this.props.admin ||
      (this.props.distributorStatus &&
        this.props.distributorStatus !== "pending")
    ) {
      price = 299.99;
      caseTotal = price * config.caseNeeded;
    } else {
      price = 499.99;
      caseTotal = price * config.caseNeeded;
    }

    return (
      <div className="columns">
        <div className="column"> {config.caseNeeded} </div>
        <div className="column"> CASE - IRIS6 </div>
        <div className="column">${this.numberWithCommas(price.toFixed(2))}</div>
        <div className="column subtotal">
          ${this.numberWithCommas(caseTotal.toFixed(2))}
        </div>
      </div>
    );
  }

  renderSingleBumpers() {
    if (this.props.config.panelWidthNeeded) {
      const { config } = this.props;

      let price = 0,
        total = 0;
      if (
        this.props.admin ||
        (this.props.distributorStatus &&
          this.props.distributorStatus !== "pending")
      ) {
        price = 254.99;
        total = price * config.singleBumpers;
        sbTotal = total;
      } else {
        price = 329.99;
        total = price * config.singleBumpers;
        sbTotal = total;
      }

      if (config.singleBumpers > 0) {
        return (
          <div className="columns">
            <div className="column"> {config.singleBumpers} </div>
            <div className="column"> IRIS - FLYBAR-1 </div>
            <div className="column">
              ${this.numberWithCommas(price.toFixed(2))}
            </div>
            <div className="column subtotal">
              ${this.numberWithCommas(total.toFixed(2))}
            </div>
          </div>
        );
      }
    }
  }

  renderDualBumpers() {
    if (this.props.config.panelWidthNeeded) {
      const { config } = this.props;

      let price = 0,
        total = 0;
      if (
        this.props.admin ||
        (this.props.distributorStatus &&
          this.props.distributorStatus !== "pending")
      ) {
        price = 324.99;
        total = price * config.dualBumpers;
        dbTotal = total;
      } else {
        price = 449.99;
        total = price * config.dualBumpers;
        dbTotal = total;
      }

      if (config.dualBumpers > 0) {
        return (
          <div className="columns">
            <div className="column"> {config.dualBumpers} </div>
            <div className="column"> IRIS - FLYBAR-2 </div>
            <div className="column">
              ${this.numberWithCommas(price.toFixed(2))}
            </div>
            <div className="column subtotal">
              ${this.numberWithCommas(total.toFixed(2))}
            </div>
          </div>
        );
      }
    }
  }

  renderTotals() {
    const subTotal =
      +recCardTotal +
      +prodTotal +
      +caseTotal +
      +dbTotal +
      +sbTotal +
      +addonTotal;

    return (
      <div className="columns">
        <div className="column" />
        <div className="column" />
        <div className="column" />
        <div className="column">
          <strong> $ {this.numberWithCommas(subTotal.toFixed(2))} </strong>
          <div className="fine">*Estimated cost before tax</div>
        </div>
      </div>
    );
  }
  render() {
    return (
      <div className="quote-form">
        <div
          className="columns"
          style={{
            borderBottom: "1px solid #1d6bbf",
          }}
        >
          <div className="column"> Qty </div>
          <div className="column"> Sku </div>
          <div className="column"> Unit Cost </div>
          {/* <div className="column">
            Discount
            <span className="discount"> {this.props.config.discount} % </span>
          </div> */}
          <div className="column"> Subtotal </div>
        </div>
        {this.renderProductRow()}
        {this.renderCases()}
        {this.renderSingleBumpers()}
        {this.renderDualBumpers()}
        {/* {this.renderRecCard()} */}
        {this.renderAddons("addons")}
        {this.renderAddons("processors")}
        {this.renderTotals()}
      </div>
    );
  }
}
