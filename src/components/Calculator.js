import React, { Component } from "react";
// import { Link } from "react-router-dom";
import _ from "lodash";

import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import { withStyles } from "@material-ui/core/styles";
import styles from "../assets/Styles";

import WidthInput from "./WidthInput";
import HeightInput from "./HeightInput";
import RatioInput from "./RatioInput";
import DiagonalInput from "./DiagonalInput";
import CustomerDetails from "./CustomerDetails";

class Calculator extends Component {
  constructor() {
    super();
    this.state = {
      userPixels: 0,
    };
  }

  renderProductSelect() {
    return _.map(this.props.item, (item, i) => {
      return (
        <MenuItem key={i} value={item.productName}>
          {item.productName}
        </MenuItem>
      );
    });
  }

  renderSaveButton() {
    const config = this.props.config,
      isInvalid =
        config.custName === "" ||
        !config.currentProd.sku ||
        config.saveName === "";
    if (this.props.user) {
      if (config.saveLoad) {
        return (
          <button
            disabled={isInvalid}
            style={{ width: "50%" }}
            className="button is-primary"
            onClick={this.props.saveForm}
          >
            Save
          </button>
        );
      } else {
        return (
          <button
            disabled={isInvalid}
            style={{ width: "50%" }}
            className="button is-primary"
            onClick={this.props.saveForm}
          >
            Save As
          </button>
        );
      }
    }
  }

  renderOptions = (type) => {
    return _.map(this.props[type], (item, i) => {
      return (
        <option key={i} value={item.sku}>
          {item.sku}
        </option>
      );
    });
  };

  renderValue = (val) => {
    const addon = _.filter(this.props.addons, {
      id: val,
    });
    return addon[0].sku;
  };

  //This renders the price selection with disabled options depending on account type

  renderPrice = (input, type, i) => {
    if (
      this.props.admin ||
      (this.props.distributorStatus &&
        this.props.distributorStatus !== "Pending Dealer")
    ) {
      if (this.props.config[type][i].adjustedPrice) {
        return (
          <TextField
            style={{ margin: "0px" }}
            autoComplete="off"
            type="number"
            margin="normal"
            inputProps={{
              min: input.price,
            }}
            value={input.price}
            onChange={this.props.changeInputValue("adjustedPrice", i, type)}
          />
        );
      } else if (this.props.config[type][i].name === "None") {
        return false;
      } else if (
        this.props.admin ||
        (this.props.distributorStatus &&
          this.props.distributorStatus !== "Pending Dealer")
      ) {
        return (
          <TextField
            style={{ margin: "0px" }}
            autoComplete="off"
            type="text"
            margin="normal"
            value={input.dealerPrice}
            onChange={this.props.changeInputValue("dealerPrice", i, type)}
          />
        );
      }
    } else {
      return (
        <TextField
          style={{ margin: "0px" }}
          autoComplete="off"
          type="number"
          margin="normal"
          disabled
          value={input.price}
        />
      );
    }
  };

  renderRemove = (i, length, type) => {
    const index = parseInt(i, 10);
    if (index !== 0) {
      return (
        <div
          style={{ padding: "0 8px", width: "0px" }}
          className="btn"
          onClick={this.props.removeAddon(type, i)}
        >
          -
        </div>
      );
    }
  };

  renderAdd = (i, length, type) => {
    const index = parseInt(i, 10);
    if (index === length) {
      return (
        <div
          style={{ padding: "0 8px" }}
          className="btn-add"
          onClick={() => this.props.appendInput(type, i)}
        >
          +
        </div>
      );
    }
  };

  renderProductSelects = (type, title) => {
    const config = this.props.config,
      length = Object.keys(config[type]).length - 1;
    if (config.currentProd.sku) {
      return (
        <div className="form-section addon-selectors">
          <h3>Select {title}</h3>
          <div className="label-flex">
            <div style={{ width: "130px", marginRight: "10px" }}>Product</div>
            <div style={{ width: "35px", marginRight: "10px" }}>Qty.</div>
            <div style={{ width: "70px" }}>Price</div>
            <div />
          </div>
          {_.map(config[type], (input, i) => {
            return (
              <div key={input.id} className="add-container">
                <div style={{ display: "table", marginRight: "10px" }}>
                  <select
                    style={{ width: "130px" }}
                    value={input.sku}
                    onChange={this.props.handleAddonSelect(type, "value", i)}
                  >
                    <option value="Select Addon">Select {title}</option>
                    {this.renderOptions(type)}
                  </select>
                </div>
                <div style={{ width: "35px", marginRight: "10px" }}>
                  <TextField
                    id="standard-number"
                    style={{ margin: "0px" }}
                    autoComplete="off"
                    type="text"
                    margin="normal"
                    value={input.qty}
                    onChange={this.props.changeInputValue("qty", i, type)}
                  />
                </div>
                <div style={{ width: "70px" }}>
                  {this.renderPrice(input, type, i)}
                </div>

                {this.renderRemove(i, length, type)}
                {this.renderAdd(i, length, type)}
              </div>
            );
          })}
        </div>
      );
    }
  };

  renderDimensions() {
    const config = this.props.config;
    const { classes } = this.props;

    if (config.currentProd.sku) {
      return (
        <FormControl>
          <h3>Select Dimensions</h3>

          <Select
            autoWidth={false}
            className={classes.select}
            value={config.dimSelect}
            onChange={this.props.handleDimSelect("dimSelect")}
          >
            <MenuItem value="Width & Height">Width & Height</MenuItem>
            <MenuItem value="Width & Diagonal">Width & Diagonal</MenuItem>
            <MenuItem value="Height & Diagonal">Height & Diagonal</MenuItem>
            <MenuItem value="Diagonal & Ratio">Diagonal & Ratio</MenuItem>
            <MenuItem value="Ratio & Width">Ratio & Width</MenuItem>
            <MenuItem value="Ratio & Height">Ratio & Height</MenuItem>
          </Select>
        </FormControl>
      );
    }
  }
  renderFormat() {
    const config = this.props.config;

    if (config.currentProd.sku) {
      return (
        <div className="radio-input-group">
          <FormControl>
            <h3>Measurement Unit</h3>
            <RadioGroup
              aria-label="Gender"
              name="gender1"
              className="radio-group"
              value={config.format}
              onChange={this.props.handleRadio("format")}
            >
              <FormControlLabel value="Ft." control={<Radio />} label="Ft." />

              <FormControlLabel value="In." control={<Radio />} label="In." />

              <FormControlLabel value="cm" control={<Radio />} label="cm" />
              <FormControlLabel value="mm" control={<Radio />} label="mm" />
            </RadioGroup>
          </FormControl>
        </div>
      );
    }
  }

  renderNumberInputs() {
    const config = this.props.config;
    if (config.currentProd.sku) {
      return (
        <div className="input-styles">
          <div className="input-halves">
            <TextField
              autoComplete="off"
              id="standard-number"
              type="number"
              label="Complete Panels"
              margin="normal"
              value={config.completePanels}
              onChange={this.props.handleInputChange("completePanels")}
            />
            <TextField
              autoComplete="off"
              id="standard-number"
              type="number"
              label="NovaStar® VX4S Processors"
              margin="normal"
              value={parseInt(config.novaStar, 10)}
              onChange={this.props.handleInputChange("novaStar")}
            />
          </div>

          <div className="input-halves">
            <TextField
              autoComplete="off"
              id="standard-number"
              type="number"
              label="Receiving Cards"
              margin="normal"
              value={config.recCards}
              onChange={this.props.handleInputChange("recCards")}
            />
            <TextField
              autoComplete="off"
              id="standard-number"
              type="number"
              label="Single Bumper / Flybars"
              margin="normal"
              value={config.singleFly}
              onChange={this.props.handleInputChange("singleFly")}
            />
          </div>
          <div className="input-halves">
            <TextField
              autoComplete="off"
              id="standard-number"
              type="number"
              label="Double Bumper / Flybars"
              margin="normal"
              value={config.doubleFly}
              onChange={this.props.handleInputChange("doubleFly")}
            />
            <TextField
              autoComplete="off"
              id="standard-number"
              type="number"
              label="Flight Case"
              margin="normal"
              value={config.ataSpec}
              onChange={this.props.handleInputChange("ataSpec")}
            />
          </div>
        </div>
      );
    }
  }

  renderInputs() {
    const config = this.props.config;

    if (config.dimSelect === "Width & Height") {
      return (
        <div>
          <WidthInput
            dimSelect={config.dimSelect}
            config={config}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
          <HeightInput
            dimSelect={config.dimSelect}
            config={config}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
          {config.dimError && (
            <div className="dim-error">{config.dimError}</div>
          )}
        </div>
      );
    } else if (config.dimSelect === "Width & Diagonal") {
      return (
        <div>
          <WidthInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
          <DiagonalInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />

          {config.dimError && (
            <div className="dim-error">{config.dimError}</div>
          )}
        </div>
      );
    } else if (config.dimSelect === "Height & Diagonal") {
      return (
        <div>
          <HeightInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
          <DiagonalInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
          {config.dimError && (
            <div className="dim-error">{config.dimError}</div>
          )}
        </div>
      );
    } else if (config.dimSelect === "Diagonal & Ratio") {
      return (
        <div>
          <RatioInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
          <DiagonalInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
        </div>
      );
    } else if (config.dimSelect === "Ratio & Height") {
      return (
        <div>
          <RatioInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
          <HeightInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
        </div>
      );
    } else if (config.dimSelect === "Ratio & Width") {
      return (
        <div>
          <RatioInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
          <WidthInput
            config={config}
            dimSelect={config.dimSelect}
            clearState={this.props.clearState}
            handleInputChange={this.props.handleInputChange}
          />
        </div>
      );
    }
  }

  numberWithCommas = (x) => {
    if (x !== undefined) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  };

  renderSuccess = (pixelTest) => {
    if (pixelTest) {
      return (
        <i
          style={{ paddingLeft: "10px" }}
          className="fas fa-check-circle success"
        />
      );
    } else {
      return (
        <i
          style={{ paddingLeft: "10px" }}
          className="far fa-exclamation-circle warning"
        />
      );
    }
  };
  renderProcessing() {
    const { config } = this.props;
    if (config.currentProd.sku) {
      let userPixels = 0;
      //eslint-disable-next-line
      const pixels = _.map(config.processors, (o, i) => {
        if (o.name !== "None") {
          const currentProcessorPixels = o.qty * o.maxPixels;
          userPixels = +currentProcessorPixels + +userPixels;
        }
      });

      const pixelTest = userPixels >= config.pixelsPerPanel;
      return (
        <div className="processing">
          <div>
            Total Pixels Needed:
            <span>
              {this.numberWithCommas(config.pixelsPerPanel)}
              {this.renderSuccess(pixelTest)}
            </span>
          </div>
          <div>
            Current Processing Power:
            <span>{this.numberWithCommas(userPixels)}</span>
          </div>
        </div>
      );
    }
  }
  render() {
    const { handleTab, config, classes } = this.props;
    return (
      <div className="inner">
        <form>
          <div className="form-section">
            <h3>Select a Product</h3>
            <FormControl>
              <Select
                autoWidth={false}
                // renderValue={() => this.renderValue(config.currentProd)}
                className={classes.select}
                value={config.productSelected}
                onChange={this.props.handleSelect("productSelected")}
              >
                <MenuItem value="Select a Product">No Item Selected</MenuItem>
                {this.renderProductSelect()}
              </Select>
            </FormControl>
          </div>
          <div className="form-section">
            {this.renderDimensions()}

            <div className="input-styles">
              {config.currentProd.sku && (
                <div>
                  {this.renderFormat()}
                  <h3>Requested Dimensions</h3>
                  {this.renderInputs()}
                </div>
              )}
            </div>
            {this.renderProductSelects("addons", "Add-Ons")}
            {this.renderProductSelects("processors", "Processors")}
            {_.map(config.addons, (addon) => {})}
            {this.renderProcessing()}
          </div>

          <CustomerDetails
            admin={this.props.admin}
            classes={classes}
            config={this.props.config}
            handleInputChange={this.props.handleInputChange}
          />
          <div className="view-controls">
            {this.props.config.widthSelected && (
              <div
                style={{
                  width: "100%",
                  textAlign: "center",
                  marginBottom: "15px",
                  marginRight: "0px",
                }}
                className="view-quote"
                onClick={handleTab("quoteTab")}
              >
                View Quote
              </div>
            )}

            {this.renderSaveButton()}
            <div
              className="button is-outlined"
              style={{ width: "50%" }}
              onClick={this.props.openDialog}
            >
              New Project
            </div>
          </div>
        </form>
      </div>
    );
  }
}
export default withStyles(styles)(Calculator);
