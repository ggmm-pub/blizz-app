import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const config = {
  apiKey: "AIzaSyApYoJGukNxGZyn3UBprNUIxxNg6EDhm5k",
  authDomain: "iriscalc.firebaseapp.com",
  databaseURL: "https://iriscalc.firebaseio.com",
  projectId: "iriscalc",
  storageBucket: "",
  messagingSenderId: "535527900321",
  appId: "1:535527900321:web:c27c44a844821cd6"
};

const fire = firebase.initializeApp(config);
export { fire };

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const auth = firebase.auth();

export { auth };

export const provider = new firebase.auth.GoogleAuthProvider();
