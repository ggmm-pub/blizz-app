import * as auth from "./auth";
import * as firebase from "./firebase";
import { fire } from "./firebase";

export { auth, firebase, fire };
