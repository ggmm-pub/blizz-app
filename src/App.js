import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import convert from "convert-units";

import NavBar from "./components/NavBar";
import Home from "./components/Home";
import Settings from "./components/Settings";
import Login from "./components/auth/Login";
import SavedData from "./components/SavedData";
import Distributors from "./components/Distributors";
import SavedDataAdmin from "./components/SavedDataAdmin";

import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

import _ from "lodash";
import KeyboardEventHandler from "react-keyboard-event-handler";

import { PageView, initGA, Event } from "./components/Tracking";

import { auth, provider, fire } from "./firebase/firebase";

class App extends Component {
  constructor() {
    const uniqueId = +new Date();
    super();
    this.state = {
      authUser: null,
      admin: false,
      allSaves: {},
      saves: [],
      videoEnabled: false,
      openedTab: "overviewTab",
      uid: "",
      item: [],
      snackbar: false,
      saveDialog: false,
      linkDialog: false,
      isSaveModalOpen: false,
      dimMode: true,

      config: {
        addons: {
          0: {
            name: "None",
            sku: "None",
            value: "None",
            qty: 1,
            price: 0,
            dealerPrice: 0,
            id: uniqueId,
          },
        },
        processors: {
          0: {
            name: "None",
            sku: "None",
            price: 0,
            dealerPrice: 0,
            qty: 1,
            id: uniqueId + 1,
            value: 0,
          },
        },
        addonProd: [],
        completePanels: 0,
        caseNeeded: 0,
        pixelsPerPanel: 0,
        currentPrice: 0,
        novaStar: 0,
        recCards: 0,
        dualBumpers: 0,
        singleBumpers: 0,
        singleFly: 0,
        doubleFly: 0,
        ataSpec: 0,
        saveLoad: false,
        saveId: null,
        linkId: null,
        dimSelect: "Width & Height",
        saveName: null,
        productSelected: "Select a Product",
        currentProd: {
          productIcon: {
            url:
              "https://firebasestorage.googleapis.com/v0/b/blizz-quote.appspot.com/o/flamelink%2Fmedia%2F1541094451536_test_product2.svg?alt=media&token=e85db290-97d8-4b3d-956a-f1ecfc286b25",
          },
        },
        dimTotal: 0,
        heightChecked: false,
        widthChecked: false,
        diagChecked: false,
        pixelHeight: 0,
        pixelWidth: 0,
        powerConsumption: 0,
        natResWidth: 0,
        natResHeight: 0,
        hd: "",
        cirNeeded: 0,
        conNeeded: 0,
        ratioChecked: false,
        widthSelected: null,
        heightSelected: null,
        diagSelected: 0,
        ratio: 0,
        totalWeight: 0,

        custName: "",
        yourName: "",
        discount: 0,
        price: 0,
        panelWidthNeeded: 0,
        panelHeightNeeded: 0,
        totalPanelsNeeded: 0,
      },
    };

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  //Creates new select on click
  handleSelect = (name) => (event) => {
    fire
      .database()
      .ref("/products")
      .once("value")
      .then((snapshot) => {
        const index = snapshot
          .val()
          .findIndex((p) => p.productName === event.target.value);

        this.setState({
          ...this.state,
          dimMode: true,
          config: {
            ...this.state.config,
            format: "Ft.",
            currentProd: snapshot.val()[index],
            [name]: event.target.value,
            currentPrice: snapshot.val()[index].price,
          },
        });
      });
  };

  appendInput = (type, i) => {
    const newInput = {
        name: "None",
        value: "None",
        qty: 1,
        price: 0,
      },
      index = parseFloat(i, 10) + 1;

    this.setState((prevState) => ({
      ...this.state,
      config: {
        ...this.state.config,
        [type]: {
          [index]: newInput,
          ...this.state.config[type],
        },
      },
    }));
  };

  closeDialog = (name) => (event) => {
    console.log("closed");
    this.setState({
      [name]: false,
    });
  };

  //Triggers when add-on select changes

  handleAddonSelect = (type, name, i) => (e) => {
    const addon = _.filter(this.state[type], {
      sku: e.target.value,
    });

    if (type === "addons") {
      console.log(addon);
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          [type]: {
            ...this.state.config[type],
            [i]: {
              ...this.state[type][i],
              [name]: e.target.value,
              sku: e.target.value,
              qty: 1,
              value: e.target.value,
              price: addon[0].price,
              dealerPrice: addon[0].dealerPrice,
            },
          },
        },
      });
    } else if (type === "processors") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          [type]: {
            ...this.state.config[type],
            [i]: {
              ...this.state[type][i],
              [name]: e.target.value,
              sku: e.target.value,
              qty: 1,
              maxPixels: addon[0].maxPixels,
              price: addon[0].price,
              dealerPrice: addon[0].dealerPrice,
            },
          },
        },
      });
    }
  };

  //Remove add-on on click

  removeAddon = (type, index) => (e) => {
    const { config } = this.state,
      removed = _.filter(config[type], (x, i) => i !== index);

    this.setState({
      ...this.state,
      config: {
        ...this.state.config,
        [type]: {
          ...removed,
        },
      },
    });
  };

  //Handle format radios

  handleRadio = (name) => (e) => {
    const targetUnit = e.target.value,
      { config } = this.state,
      { heightSelected, widthSelected, format } = config;

    if (targetUnit === "In." && format === "Ft.") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("ft")
            .to("in")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("ft").to("in").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "Ft." && format === "In.") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("in")
            .to("ft")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("in").to("ft").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "cm" && format === "Ft.") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("ft")
            .to("cm")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("ft").to("cm").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "Ft." && format === "cm") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("cm")
            .to("ft")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("cm").to("ft").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "In." && format === "cm") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("cm")
            .to("in")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("cm").to("in").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "cm" && format === "In.") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("in")
            .to("cm")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("in").to("cm").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "cm" && format === "mm") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("mm")
            .to("cm")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("mm").to("cm").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "mm" && format === "cm") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("cm")
            .to("mm")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("cm").to("mm").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "Ft." && format === "mm") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("mm")
            .to("ft")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("mm").to("ft").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "mm" && format === "Ft.") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("ft")
            .to("mm")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("ft").to("mm").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "mm" && format === "In.") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("in")
            .to("mm")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("in").to("mm").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
    if (targetUnit === "In." && format === "mm") {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          heightSelected: convert(heightSelected)
            .from("mm")
            .to("in")
            .toFixed(2),
          widthSelected: convert(widthSelected).from("mm").to("in").toFixed(2),
          [name]: e.target.value,
        },
      });
    }
  };

  //Triggers panel background video on click

  enableVideo = () => (event) => {
    if (this.state.videoEnabled) {
      this.setState({
        videoEnabled: false,
      });
    } else {
      this.setState({
        videoEnabled: true,
      });
    }
  };

  //Trigger snackbar callback w/ delay

  delayClose = () => {
    setTimeout(
      function () {
        this.setState({ snackbar: false });
      }.bind(this),
      3000
    );
  };

  //Reset form

  clearState = (name) => (event) => {
    this.setState({
      ...this.state,
      dimMode: false,
      config: {
        ...this.state.config,
        [name]: "",
      },
    });
  };

  //Generic dialog functions

  openDialog = () => {
    this.setState({ saveDialog: true });
  };

  //Gets data from Firebase for read-only links

  readOnlyLink = () => (uid, saveid) => {
    fire
      .database()
      .ref("/users/" + uid)
      .child("savedData")
      .child(saveid)
      .once("value")
      .then((snapshot) => {
        this.setState({
          config: snapshot.val(),
        });
      });
  };

  //Resets form when clicked // Should refactor with INIT STATE

  resetForm = () => {
    window.history.pushState("object or string", "Title", "/editor/");
    this.setState({
      allSaves: {},
      saves: [],
      videoEnabled: false,
      openedTab: "overviewTab",
      snackbar: false,
      uid: this.state.uid,
      saveDialog: false,
      dimMode: true,
      config: {
        addons: {
          0: {
            name: "None",
            sku: "None",
            value: "None",
            qty: 1,
            price: 0,
            dealerPrice: 0,
          },
        },
        processors: {
          0: {
            name: "None",
            sku: "None",
            price: 0,
            qty: 1,
            value: 0,
            dealerPrice: 0,
          },
        },
        completePanels: 0,
        caseNeeded: 0,
        pixelsPerPanel: 0,
        currentPrice: 0,
        novaStar: 0,
        recCards: 0,
        singleBumpers: 0,
        dualBumpers: 0,
        singleFly: 0,
        doubleFly: 0,
        ataSpec: 0,
        saveLoad: false,
        saveId: null,
        linkId: null,
        dimSelect: "Width & Height",
        saveName: null,
        productSelected: "Select a Product",
        currentProd: {
          productIcon: {
            url:
              "https://firebasestorage.googleapis.com/v0/b/blizz-quote.appspot.com/o/flamelink%2Fmedia%2F1541094451536_test_product2.svg?alt=media&token=e85db290-97d8-4b3d-956a-f1ecfc286b25",
          },
        },
        dimTotal: 0,
        heightChecked: false,
        widthChecked: false,
        diagChecked: false,
        pixelHeight: 0,
        pixelWidth: 0,
        powerConsumption: 0,
        natResWidth: 0,
        natResHeight: 0,
        hd: "",
        cirNeeded: 0,
        conNeeded: 0,
        ratioChecked: false,
        widthSelected: 0,
        heightSelected: 0,
        diagSelected: 0,
        ratio: 0,
        ratioSelected_1: 0,
        ratioSelected_2: 0,
        totalWeight: 0,

        custName: "",
        yourName: "",
        discount: 0,
        price: 0,
        panelWidthNeeded: 0,
        panelHeightNeeded: 0,
        totalPanelsNeeded: 0,
      },
    });
  };

  //Sets dimMode false

  handleDimSelect = (name) => (event) => {
    this.setState(
      {
        ...this.state,
        dimMode: false,
        config: {
          ...this.state.config,
          [name]: event.target.value,
        },
      },
      () => this.handleCalc()
    );
  };

  //Adjusts pricing on number inputs

  changePrice = (name, data) => (event) => {
    this.setState({
      config: {
        ...this.state.config,
        [name]: {
          ...this.state.config[name],
          adjustedPrice: event.target.value,
        },
      },
    });
  };

  handleQty = (name) => (event) => {
    this.setState({
      config: {
        ...this.state.config,
        [name]: {
          ...this.state.config[name],
          qty: event.target.value,
        },
      },
    });
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({ snackbar: false });
  };

  handleChecked = (name) => (event) => {
    const target = event.target.checked;

    this.setState({
      config: {
        ...this.state.config,
        [name]: target,
      },
    });
  };

  saveCallback = () => {
    this.setState(
      {
        isSaveModalOpen: false,
        snackbar: true,
        config: {
          ...this.state.config,
          saveLoad: true,
        },
      },
      () => this.delayClose()
    );
  };

  saveModal = (name) => (e) => {
    this.setState({
      isSaveModalOpen: true,
    });
  };

  saveForm = (name) => (event) => {
    Event("SAVE", "Form Saved", "MAIN_APP");

    const data = this.state.config,
      uniqueId = +new Date(),
      url = window.location.pathname;
    let idx = url.lastIndexOf("/");
    event.preventDefault();
    const saveId = url.substring(idx + 1).trim();
    if (url === "/editor" || url === "/editor/") {
      window.history.pushState(
        "object or string",
        "Title",
        "/editor/" + this.state.uid + "/" + uniqueId
      );
      fire
        .database()
        .ref("/users/" + this.state.uid)
        .update({
          hasSave: true,
        });
      fire
        .database()
        .ref("/users/" + this.state.uid + "/savedData/" + uniqueId)
        .set(
          {
            format: data.format,
            addons: data.addons,
            processors: data.processors,
            saveLoad: true,
            dimSelect: data.dimSelect,
            caseNeeded: data.caseNeeded,
            cirNeeded: data.cirNeeded,
            conNeeded: data.conNeeded,
            natResWidth: data.natResWidth,
            natResHeight: data.natResHeight,
            hd: data.hd,
            productSelected: data.productSelected,
            id: uniqueId,
            totalWeight: data.totalWeight,
            currentPrice: data.currentPrice,
            pixelsPerPanel: data.pixelsPerPanel,
            currentProd: data.currentProd,
            saveName: data.saveName,
            pixelHeight: data.pixelHeight,
            pixelWidth: data.pixelWidth,
            powerConsumption: data.powerConsumption,
            widthSelected: data.widthSelected,
            heightSelected: data.heightSelected,
            diagSelected: data.diagSelected,
            ratio: data.ratio,
            ratioSelected_1: data.ratioSelected_1,
            ratioSelected_2: data.ratioSelected_2,
            completePanels: data.completePanels,
            novaStar: data.novaStar,
            recCards: data.recCards,
            singleBumpers: data.singleBumpers,
            dualBumpers: data.dualBumpers,
            singleFly: data.singleFly,
            doubleFly: data.doubleFly,
            ataSpec: data.ataSpec,
            custName: data.custName,
            yourName: data.yourName,
            discount: data.discount,
            price: data.price,
            panelWidthNeeded: data.panelWidthNeeded,
            panelHeightNeeded: data.panelHeightNeeded,
            totalPanelsNeeded: data.totalPanelsNeeded,
          },
          () => this.saveCallback()
        );
    } else {
      fire
        .database()
        .ref("/users/" + this.state.uid + "/savedData/" + saveId)
        .update(
          {
            format: data.format,
            addons: data.addons,
            processors: data.processors,
            saveLoad: true,
            dimSelect: data.dimSelect,
            caseNeeded: data.caseNeeded,
            cirNeeded: data.cirNeeded,
            conNeeded: data.conNeeded,
            natResWidth: data.natResWidth,
            natResHeight: data.natResHeight,
            hd: data.hd,
            productSelected: data.productSelected,
            id: saveId,
            totalWeight: data.totalWeight,
            currentPrice: data.currentPrice,
            pixelsPerPanel: data.pixelsPerPanel,
            currentProd: data.currentProd,
            saveName: data.saveName,
            pixelHeight: data.pixelHeight,
            pixelWidth: data.pixelWidth,
            powerConsumption: data.powerConsumption,
            widthSelected: data.widthSelected,
            heightSelected: data.heightSelected,
            diagSelected: data.diagSelected,
            ratio: data.ratio,
            ratioSelected_1: data.ratioSelected_1,
            ratioSelected_2: data.ratioSelected_2,
            completePanels: data.completePanels,
            novaStar: data.novaStar,
            recCards: data.recCards,
            singleFly: data.singleFly,
            doubleFly: data.doubleFly,
            ataSpec: data.ataSpec,
            custName: data.custName,
            yourName: data.yourName,
            discount: data.discount,
            price: data.price,
            panelWidthNeeded: data.panelWidthNeeded,
            panelHeightNeeded: data.panelHeightNeeded,
            totalPanelsNeeded: data.totalPanelsNeeded,
          },
          () => this.saveCallback()
        );
    }
  };

  removeSave = (e) => (id) => {
    //eslint-disable-next-line
    const uid = _.filter(this.state.saves, { uid: id });
    console.log(uid);
    fire
      .database()
      .ref("/users/" + this.state.uid + "/savedData/" + id)
      .remove()
      .catch((error) => {
        console.log(error);
      })
      .then(
        this.setState({
          ...this.state,
          [id]: true,
        })
      );
  };

  handleTab = (name) => (event) => {
    this.setState({
      openedTab: name,
    });
  };

  handleSaveProduct = (data, type) => (event) => {
    fire
      .database()
      .ref("/" + type)
      .set(data)
      .then(() =>
        this.setState(
          {
            snackbar: true,
          },
          () => this.delayClose()
        )
      );
  };
  handleSaveAddons = (type) => (event) => {
    fire
      .database()
      .ref("/" + type)
      .set(this.state.addons)
      .then(() =>
        this.setState(
          {
            snackbar: true,
          },
          () => this.delayClose()
        )
      );
  };

  handleInputChange = (name, type) => (event) => {
    if (event.target.value >= 0) {
      console.log("input changed");
      this.setState(
        {
          ...this.state,
          dimMode: false,
          config: {
            ...this.state.config,

            [name]: event.target.value,
          },
        },
        () => this.handleCalc(type)
      );
    } else {
      this.setState({
        ...this.state,
        config: {
          ...this.state.config,
          [name]: event.target.value,
        },
      });
    }
  };

  //Generic debounce w/ type

  //Calculate X & Y from Diag + Ratio
  calcX = (d, n, m) => {
    return (d * m) / Math.sqrt(m * m + n * n);
  };

  calcY = (d, n, m) => {
    return (d * n) / Math.sqrt(m * m + n * n);
  };

  //Generic input handler ** causes errors

  changeInputValue = (name, i, type) => (e) => {
    this.setState({
      ...this.state,
      config: {
        ...this.state.config,
        [type]: {
          ...this.state.config[type],
          [i]: {
            ...this.state.config[type][i],
            [name]: e.target.value,
          },
        },
      },
    });
  };

  //handles all calculations

  handleCalc = (type) => {
    const state = this.state.config;
    let heightSelected = state.heightSelected,
      widthSelected = state.widthSelected,
      diagSelected = state.diagSelected,
      ratio1 = state.ratioSelected_1,
      ratio2 = state.ratioSelected_2,
      ratioSelected = ratio1 / ratio2,
      prod = this.state.config.currentProd;

    function solveForLeg(leg, hypotenuse) {
      return Math.sqrt(Math.pow(hypotenuse, 2) - Math.pow(leg, 2));
    }

    function solveForHypotenuse(leg1, leg2) {
      return Math.sqrt(Math.pow(leg1, 2) + Math.pow(leg2, 2));
    }
    function solveForDiagonal(w, r) {
      const widthSqr = w / r;
      return Math.sqrt(Math.pow(widthSqr, 2) + Math.pow(w, 2));
    }

    //Calculates height and aspect ratio

    if (diagSelected > 0 && widthSelected > 0 && type === "Width & Diagonal") {
      let height = solveForLeg(widthSelected, diagSelected);

      if (parseFloat(diagSelected) < parseFloat(widthSelected)) {
        this.setState({
          config: {
            ...this.state.config,
            dimError: "The diagonal length must be greater than the width",
            panelHeightNeeded: Math.ceil(
              (heightSelected * 12 * 25.3) / prod.height
            ),
            heightSelected: Math.max(Math.ceil(height * 100) / 100),
          },
        });
      }
      if (parseFloat(diagSelected) > parseFloat(widthSelected)) {
        this.setState(
          {
            config: {
              ...this.state.config,
              dimError: null,
              panelHeightNeeded: Math.ceil(
                (heightSelected * 12 * 25.3) / prod.height
              ),
              heightSelected: Math.max(Math.ceil(height * 100) / 100),
            },
          },
          () => this.calcTotals()
        );
      }
    } else if (
      diagSelected > 0 &&
      heightSelected > 0 &&
      type === "Height & Diagonal"
    ) {
      let width = solveForLeg(heightSelected, diagSelected);
      if (parseFloat(diagSelected) < parseFloat(heightSelected)) {
        this.setState(
          {
            config: {
              ...this.state.config,
              dimError: "The diagonal length must be greater than the height",
              panelWidthNeeded: Math.ceil(
                (widthSelected * 12 * 25.3) / prod.width
              ),
              widthSelected: Math.max(Math.ceil(width * 100) / 100),
            },
          },
          () => this.calcTotals()
        );
      } else if (parseFloat(diagSelected) > parseFloat(heightSelected)) {
        this.setState(
          {
            config: {
              ...this.state.config,
              dimError: null,
              panelWidthNeeded: Math.ceil(
                (widthSelected * 12 * 25.3) / prod.width
              ),
              widthSelected: Math.max(Math.ceil(width * 100) / 100),
            },
          },
          () => this.calcTotals()
        );
      }
    } else if (
      heightSelected > 0 &&
      widthSelected > 0 &&
      type === "Width & Height"
    ) {
      let hypotenuse = solveForHypotenuse(heightSelected, widthSelected);

      this.setState(
        {
          config: {
            ...this.state.config,
            diagSelected: Math.max(Math.ceil(hypotenuse * 100) / 100),
            ratioSelected_1: widthSelected / heightSelected,
            ratioSelected_2: 1,
          },
        },
        () => this.calcTotals()
      );
    } else if (
      widthSelected > 0 &&
      ratio1 > 0 &&
      ratio2 > 0 &&
      type === "Ratio & Width"
    ) {
      const ratio = widthSelected / ratio1; //0.545
      let height = ratio * ratio2;

      const diagonal = solveForDiagonal(widthSelected, ratioSelected);
      this.setState(
        {
          config: {
            ...this.state.config,
            diagSelected: diagonal.toFixed(2),
            heightSelected: height.toFixed(2),
          },
        },
        () => this.calcTotals()
      );
    } else if (
      diagSelected > 0 &&
      //m
      ratio1 > 0 &&
      //n
      ratio2 > 0 &&
      type === "Diagonal & Ratio"
    ) {
      const x = this.calcX(diagSelected, ratio2, ratio1),
        y = this.calcY(diagSelected, ratio2, ratio1),
        w = x.toFixed(2),
        h = y.toFixed(2);
      this.setState(
        {
          config: {
            ...this.state.config,
            widthSelected: w,
            heightSelected: h,
          },
        },
        () => this.calcTotals()
      );
    } else if (
      heightSelected > 0 &&
      ratioSelected > 0 &&
      type === "Ratio & Height"
    ) {
      const ratio = heightSelected / ratio2;
      const width = ratio * ratio1;

      let hypotenuse = solveForHypotenuse(heightSelected, widthSelected);
      this.setState(
        {
          config: {
            ...this.state.config,
            diagSelected: hypotenuse.toFixed(2),
            widthSelected: width.toFixed(2),
          },
        },
        () => this.calcTotals()
      );
    }
  };

  //Main calculator function
  calcTotals = () => {
    const { config } = this.state,
      prod = config.currentProd;

    //total panels
    let panelsWide = Math.ceil(
        (config.widthSelected * 25.3) / parseFloat(prod.width)
      ),
      panelsHigh = Math.ceil(
        (config.heightSelected * 25.3) / parseFloat(prod.height)
      );

    if (config.format === "Ft.") {
      panelsWide = Math.ceil(
        (config.widthSelected * 12 * 25.3) / parseFloat(prod.width)
      );
      panelsHigh = Math.ceil(
        (config.heightSelected * 12 * 25.3) / parseFloat(prod.height)
      );
    } else if (config.format === "cm") {
      panelsWide = Math.ceil(
        (config.widthSelected * 0.393 * 25.3) / parseFloat(prod.width)
      );
      panelsHigh = Math.ceil(
        (config.heightSelected * 0.393 * 25.3) / parseFloat(prod.height)
      );
    } else if (config.format === "mm") {
      panelsWide = Math.ceil(
        (config.widthSelected * 0.0393 * 25.3) / parseFloat(prod.width)
      );
      panelsHigh = Math.ceil(
        (config.heightSelected * 0.0393 * 25.3) / parseFloat(prod.height)
      );
    }

    //Bumpers
    const dualBumpers = panelsWide / 2;
    let singleBumpers = !Number.isInteger(dualBumpers) ? 1 : 0;

    let flyBar1Weight = _.filter(this.state.addons, (addon) => {
        return addon.sku === "IRIS-FLYBAR-1";
      }),
      flyBar2Weight = _.filter(this.state.addons, (addon) => {
        return addon.sku === "IRIS-FLYBAR-2";
      }),
      flyBar1TotalWeight = flyBar1Weight[0].weight * config.singleBumpers,
      flyBar2TotalWeight = flyBar2Weight[0].weight * config.dualBumpers;

    let totalPanels = panelsWide * panelsHigh,
      //weight

      weight =
        totalPanels * prod.weight +
        parseFloat(flyBar2TotalWeight) +
        parseFloat(flyBar1TotalWeight),
      totalWeight = weight.toFixed(1),
      //pixels per panel
      pixels = prod.pixelsPerPanel * totalPanels,
      //power Consumption
      //divided BY 4
      power = (prod.powerConsumption / 4) * totalPanels,
      //native resolution
      natResWidth = prod.pixelWidth * panelsWide,
      natResHeight = prod.pixelHeight * panelsHigh,
      //circuits needed
      cirNeeded = Math.ceil(power / 1800),
      //controllers needed
      conNeeded = Math.ceil(pixels / 1200000);
    //native hd check
    let hd = null;
    //input 2048x1664
    if (natResWidth < 1280 || natResHeight < 720) {
      hd = "No";
    } else if (
      natResWidth >= 1280 &&
      natResWidth < 1920 &&
      natResHeight >= 720 &&
      natResHeight < 1080
    ) {
      hd = "720p"; // 1280x720
    } else if (
      natResWidth >= 1920 &&
      natResWidth < 2560 &&
      natResHeight >= 1080 &&
      natResHeight < 1440
    ) {
      hd = "1080p"; // 1920x1080
    } else if (
      natResWidth >= 2560 &&
      natResWidth < 3840 &&
      natResHeight >= 1440 &&
      natResHeight < 2160
    ) {
      hd = "QHD"; // 2560x1440
    } else if (natResWidth >= 3840 && natResHeight >= 2160) {
      hd = "4K"; // 3840x2160
    } else {
      hd = "Yes";
    }
    //ratio

    let ratio = panelsWide / panelsHigh;

    //cases needed
    const caseNeeded = Math.ceil(totalPanels / prod.casePak),
      optimalProcessors = {
        ...this.state.processors[0],
        qty: conNeeded,
      };
    this.setState(
      {
        config: {
          ...this.state.config,
          processors: { 0: { ...optimalProcessors } },
          totalWeight: totalWeight,
          pixelsPerPanel: pixels,
          dualBumpers: Math.floor(dualBumpers),
          singleBumpers: singleBumpers,
          caseNeeded: caseNeeded,
          cirNeeded: cirNeeded,
          conNeeded: conNeeded,
          natResWidth: natResWidth,
          natResHeight: natResHeight,
          hd: hd,
          panelWidthNeeded: panelsWide,
          panelHeightNeeded: panelsHigh,
          totalPanelsNeeded: totalPanels,
          ratio: ratio.toFixed(2),
          pixelHeight: prod.pixelHeight,
          pixelWidth: prod.pixelWidth,
          powerConsumption: power,
        },
      },
      () => this.calcPrice()
    );
  };

  calcPrice = () => {
    const config = this.state.config,
      price = config.currentProd.price,
      totalPrice = price * config.totalPanelsNeeded;

    this.setState({
      config: {
        ...this.state.config,
        price: totalPrice,
      },
    });
  };

  componentDidMount() {
    initGA("UA-16655377-2");
    PageView();

    //Get Products
    fire
      .database()
      .ref("/products/")
      .once("value")
      .then((snapshot) => {
        this.setState({
          item: snapshot.val(),
        });
      });
    //Get Addons
    fire
      .database()
      .ref("/addons/")
      .once("value")
      .then((snapshot) => {
        this.setState({
          ...this.state,
          addons: snapshot.val(),
        });
      });

    //Get Processors
    fire
      .database()
      .ref("/processors/")
      .once("value")
      .then((snapshot) => {
        this.setState({
          ...this.state,
          processors: snapshot.val(),
        });
      });

    //Authenticate user

    auth.onAuthStateChanged((authUser) => {
      authUser
        ? fire
            .database()
            .ref("/users/" + authUser.uid)
            .once("value")
            .then((snapshot) => {
              this.setState(
                {
                  admin: snapshot.val() && snapshot.val().admin,
                  saves: snapshot.val() && snapshot.val().savedData,
                  authUser: authUser,
                  distributorStatus:
                    snapshot.val() && snapshot.val().distributorStatus,
                  uid: authUser.uid,
                },
                () => this.getAllSaves()
              );
            })
        : this.setState({ authUser: null });
    });
  }

  //Retrieve all saves for admin

  getAllSaves() {
    if (this.state.admin) {
      fire
        .database()
        .ref("/users/")
        .orderByChild("hasSave")
        .equalTo(true)
        .once("value")
        .then((snapshot) => {
          let data = [];
          // eslint-disable-next-line
          const map = _.map(snapshot.val(), (allSaves) => {
            return _.map(allSaves.savedData, (savedData) => {
              const title = savedData.saveName,
                company = allSaves.company;
              data.push({
                title: title,
                company: company,
                uid: savedData.uid,
                id: savedData.id,
              });
            });
          });
          this.setState({
            allSaves: data,
          });
        });
    }
  }

  //Generic auth functions

  logout() {
    auth.signOut().then(() => {
      this.setState(
        {
          user: null,
        },
        () => (window.location.href = "/")
      );
    });
  }
  login() {
    auth.signInWithPopup(provider).then((result) => {
      const user = result.user;
      this.setState({
        user,
      });
    });
  }

  render() {
    const { isSaveModalOpen, distributorStatus } = this.state;
    return (
      <BrowserRouter>
        <div>
          <KeyboardEventHandler
            handleKeys={["ctrl+shift+s"]}
            onKeyEvent={this.saveForm()}
          />

          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            open={this.state.snackbar}
            autoHideDuration={6000}
            ContentProps={{
              "aria-describedby": "message-id",
            }}
            message={<span id="message-id">Everything Saved</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={this.handleClose}
              >
                <CloseIcon />
              </IconButton>,
            ]}
          />

          <NavBar
            distributorStatus={this.state.distributorStatus}
            admin={this.state.admin}
            saveForm={this.saveForm}
            logout={this.logout}
            user={this.state.authUser}
          />
          <div className="mobile-alert">
            <div className="container">
              <div>
                <h3>Looks like you're on a mobile device.</h3>
                <p>
                  To edit and customize your video panel quote please view this
                  application on your desktop
                </p>
              </div>
            </div>
          </div>
          <Switch>
            <Route
              path="(/editor|/editor:id|/)"
              render={(props) => (
                <Home
                  handleQuote={this.handleQuote}
                  distributorStatus={distributorStatus}
                  isSaveModalOpen={isSaveModalOpen}
                  saveModal={this.saveModal}
                  removeAddon={this.removeAddon}
                  handleRadio={this.handleRadio}
                  admin={this.state.admin}
                  handleAddonSelect={this.handleAddonSelect}
                  handleProcessorSelect={this.handleProcessorSelect}
                  appendInput={this.appendInput}
                  appendProcessor={this.appendProcessor}
                  changeInputValue={this.changeInputValue}
                  dimMode={this.state.dimMode}
                  handleQty={this.handleQty}
                  changePrice={this.changePrice}
                  readOnlyLink={this.readOnlyLink()}
                  loadLocalStorage={this.loadLocalStorage}
                  handleTab={this.handleTab}
                  openedTab={this.state.openedTab}
                  saveDialog={this.state.saveDialog}
                  openDialog={this.openDialog}
                  closeDialog={this.closeDialog}
                  handleClose={this.handleClose}
                  snackbar={this.state.snackbar}
                  item={this.state.item}
                  addons={this.state.addons}
                  processors={this.state.processors}
                  config={this.state.config}
                  enableVideo={this.enableVideo}
                  videoEnabled={this.state.videoEnabled}
                  handleChecked={this.handleChecked}
                  clearState={this.clearState}
                  handleInputChange={this.handleInputChange}
                  handleDimSelect={this.handleDimSelect}
                  handleSelect={this.handleSelect}
                  newCalc={this.newCalc}
                  saveForm={this.saveForm}
                  resetForm={this.resetForm}
                  uid={this.state.uid}
                  user={this.state.authUser}
                  location={props.location}
                  {...props}
                />
              )}
            />
            <Route
              path="/saves"
              render={(props) => (
                <SavedData
                  admin={this.state.admin}
                  saves={this.state.saves}
                  uid={this.state.uid}
                  remove={this.removeSave()}
                  location={props.location}
                  {...props}
                />
              )}
            />
            <Route
              path="/admin"
              render={(props) => (
                <SavedDataAdmin
                  state={this.state}
                  saves={this.state.allSaves}
                  uid={this.state.uid}
                  admin={this.state.admin}
                  remove={this.removeSave()}
                  location={props.location}
                  {...props}
                />
              )}
            />
            <Route
              path="/dealers"
              render={(props) => (
                <Distributors
                  saves={this.state.allSaves}
                  uid={this.state.uid}
                  location={props.location}
                  {...props}
                />
              )}
            />
            <Route
              path="/settings"
              render={(props) => (
                <Settings
                  addons={this.state.addons}
                  processors={this.state.processors}
                  config={this.state.config}
                  handleClose={this.handleClose}
                  appendProcessor={this.appendProcessor}
                  appendAddon={this.appendAddon}
                  appendProduct={this.appendProduct}
                  snackbar={this.state.snackbar}
                  handleSaveProduct={this.handleSaveProduct}
                  handleSaveAddons={this.handleSaveAddons}
                  products={this.state.item}
                  admin={this.state.admin}
                  saves={this.state.allSaves}
                  uid={this.state.uid}
                  location={props.location}
                  {...props}
                />
              )}
            />
            <Route
              path="/login"
              render={(props) => (
                <Login
                  login={this.login}
                  user={this.state.user}
                  location={props.location}
                  {...props}
                />
              )}
            />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
