import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";

export default class WidthInput extends Component {
  render() {
    const config = this.props.config;
    return (
      <TextField
        id="width"
        label="Width"
        margin="normal"
        maxLength="3"
        value={config.widthSelected}
        onChange={this.props.handleInputChange(
          "widthSelected",
          this.props.dimSelect
        )}
        type="number"
        InputProps={{
          endAdornment: (
            <InputAdornment variant="filled" position="end">
              {config.format}
            </InputAdornment>
          )
        }}
      />
    );
  }
}
