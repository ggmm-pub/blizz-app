import React from "react";
import ReactDOM from "react-dom";
import ReactGA from "react-ga";
ReactGA.initialize("UA-000000-01");

import "./index.css";

import App from "./App";

ReactDOM.render(<App />, document.getElementById("root"));
