import React from "react";
import ReactDOM from "react-dom";
import ReactGA from "react-ga";

import "./index.css";

import App from "./App";
ReactGA.initialize("UA-16655377-2");

ReactDOM.render(<App />, document.getElementById("root"));
