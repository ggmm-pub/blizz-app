import React from "react";
import ReactDOM from "react-dom";
import ReactGA from "react-ga";
ReactGA.initialize("UA-16655377-2");

import "./index.css";

import App from "./App";

ReactDOM.render(<App />, document.getElementById("root"));
