import React from "react";
import ReactDOM from "react-dom";
import TagManager from "react-gtm-module";

import "./index.css";

import App from "./App";

const tagManagerArgs = {
  gtmId: "GTM-NHXWBWQ"
};

TagManager.initialize(tagManagerArgs);

ReactDOM.render(<App />, document.getElementById("root"));
