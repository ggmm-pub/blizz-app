import React from "react";
import ReactDOM from "react-dom";
import ReactGA from "react-ga";

import "./index.css";

import App from "./App";

ReactGA.initialize("UA-16655377-2");
ReactGA.pageview(window.location.pathname + window.location.search);

ReactDOM.render(<App />, document.getElementById("root"));
